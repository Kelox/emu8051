public class UI_Editor extends UI_Widget {
	private ArrayList<StringContainer> line;
	private int start_line;
	private float scroll;
	
	private int sel_line;
	private int sel_char;
	
	public int[] pc_nr;
	public int[] pc_line;
	public boolean mark_line;
	
	
	public UI_Editor(int px, int py, int pw, int ph) {
		super(px, py, pw, ph);
		
		line = new ArrayList<StringContainer>();
		line.add(new StringContainer());
		start_line = 0;
		scroll = 0;
		
		sel_line = 0;
		sel_char = -1;
		
		pc_nr = null;
		pc_line = null;
		mark_line = true;
	}
	
	
	public void draw() {
		translate(x, y);
		
		// Prepare...
		textAlign(LEFT, TOP);
		textSize(24);
		int max_lines = (int)floor(h / 24);
		
		// Draw Background
		strokeWeight(3);
		stroke(0);
		fill(255);
		rect(0, 0, w, h, 16);
		stroke(0, 0, 0, 0);
		fill(155, 155, 155, 155);
		float lcw = textWidth("999"); // line count width
		rect(0, 0, lcw, h, 16);
		
		
		translate(lcw, 0);
		// Draw Lines
		fill(0);
		for (int i = 0; i < max_lines; i++) {
			int id = i + start_line;
			if (id >= line.size())
				fill(0, 0, 0, 125);
			
			if (sel_line == id) {
				stroke(0, 0, 0, 0);
				fill(125, 125, 255, 50);
				rect(0, i * 24, w - lcw, 24);
				
				fill(0);
			}
			
			text("" + (id + 1), -lcw, i * 24);
			if (id < line.size()) {
				StringContainer c = line.get(id);
				
				if (c.breakpoint) {
					stroke(0, 0, 0, 0);
					fill(255, 0, 0, 200);
					ellipse(-(lcw / 2), i * 24 + 12, 20, 20);
				}
				
				// Draw Marker
				for (int k = 0; k < c.marker.size(); k++) {
					Marker m = c.marker.get(k);
					float phx = textWidth(c.str.substring(0, m.start));
					float phw = textWidth(c.str.substring(m.start, m.end + 1));
					
					fill(m.clr);
					stroke(0, 0, 0, 0);
					rect(phx, i * 24, phw, 24);
				}
				
				// Draw Text
				fill(0);
				text(c.str, 0, i * 24);
			}
		}
		
		// Draw PC
		if (mark_line && pc_nr != null) {
			for (int i = 0; i < pc_nr.length; i++) {
				if (mc.PC == pc_nr[i]) {
					if (pc_line[i] >= start_line && pc_line[i] < (start_line + line.size())) {
						stroke(0, 0, 0, 0);
						fill(255, 0, 0, 50);
						rect(0, (pc_line[i] - start_line) * 24, w - lcw, 24);
					}
					break;
				}
			}
		}
		
		// Draw Selector
		if (ui_active == this && sel_line >= start_line && sel_line < (start_line + max_lines)) {
			float t = (sin(millis() * 0.02) < 0 ? 0 : 1);
			strokeWeight(1);
			stroke(0);
			fill(0, 0, 0, 255 * t);
			float phx = 0;
			if (sel_char == -1)
				phx = textWidth(line.get(sel_line).str);
			else
				phx = textWidth(line.get(sel_line).str.substring(0, sel_char));
			
			rect(phx, (sel_line - start_line) * 24, 12, 24);
		}
		
		translate(-lcw, 0);
		translate(-x, -y);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		ui_active = this;
	}
	public void clicked(float px, float py) {
		ui_active = this;
		
		py -= y;
		int row = (int)floor(py / 24) + start_line;
		row = (row < 0 ? 0 : row);
		row = (row >= line.size() ? (line.size() - 1) : row);
		
		if (px < textWidth("999")) {
			StringContainer c = line.get(row);
			c.breakpoint = !c.breakpoint;
		}
		
		sel_char = -1;
		sel_line = row;
	}
	public void dragged(float px, float py, float dx, float dy) {
		scroll -= dy / 24;
		scroll = (scroll < 0 ? 0 : scroll);
		scroll = (scroll >= line.size() ? (line.size() - 1) : scroll);
		
		start_line = (int)floor(scroll);
	}
	public void keypressed(int k, int kcode) {
		boolean allowed = true;
		for (int i = 0; i < txt_disallowed.length; i++) {
			if (txt_disallowed[i] == kcode) {
				allowed = false;
				break;
			}
		}
		
		
		if (allowed) {
			line.get(sel_line).write(sel_char, str(char(k)));
			if (sel_char != -1)
				sel_char++;
			
			//if (str(char(k)) == " ")
			line.get(sel_line).update_marker(0);
		} else {
			switch (kcode) {
				case (TXT_BACKSPACE):
					boolean ret = line.get(sel_line).erase(sel_char);
					if (!ret && sel_char != -1)
						sel_char--;
					if (ret && sel_line > 0) {
						if (sel_char == -1) {
							line.remove(sel_line);
						} else {
							sel_char = line.get(sel_line - 1).str.length();
							line.get(sel_line - 1).str += line.get(sel_line).str;
							line.remove(sel_line);
						}
						
						sel_line--;
					}
					
					line.get(sel_line).update_marker(0);
					break;
				case (TXT_ENTER):
					StringContainer ph = new StringContainer();
					
					// Keep tabs
					String phStr = line.get(sel_line).str;
					int space_cnt = 0;
					for (int i = 0; i < phStr.length(); i++) {
						if (strcmp(phStr.charAt(i), " ")) {
							ph.str += " ";
							space_cnt++;
						} else {
							break;
						}
					}
					
					// Cut text
					if (sel_char != -1) {
						ph.str += line.get(sel_line).str.substring(sel_char);
						line.get(sel_line).str = line.get(sel_line).str.substring(0, sel_char);
					}
					
					add_line(ph, sel_line + 1);
					line.get(sel_line).update_marker(0);
					sel_line++;
					sel_char = (ph.str.length() == space_cnt ? -1 : space_cnt);
					if (line.get(sel_line).str.length() == 0)
						sel_char = -1;
					
					line.get(sel_line).update_marker(0);
					break;
				case (TXT_UP):
					if (sel_line != 0) {
						sel_line--;
						if (line.get(sel_line).str.length() == 0)
							sel_char = -1;
						else if (line.get(sel_line).str.length() <= sel_char)
							sel_char = -1;
					}
					break;
				case (TXT_DOWN):
					if (sel_line != (line.size() - 1)) {
						sel_line++;
						if (line.get(sel_line).str.length() == 0)
							sel_char = -1;
						else if (line.get(sel_line).str.length() <= sel_char)
							sel_char = -1;
					}
					break;
				case (TXT_LEFT):
					if (sel_char == -1)
						sel_char = line.get(sel_line).str.length();
					
					sel_char--;
					
					if (sel_char == -1 && sel_line > 0)
						sel_line--;
					else if (sel_char == -1 && sel_line == 0)
						sel_char = 0;
					
					break;
				case (TXT_RIGHT):
					if (sel_char == -1) {
						if ((sel_line + 1) < line.size()) {
							sel_line++;
							sel_char = 0;
							if (line.get(sel_line).str.length() == 0)
								sel_char = -1;
						}
					} else {
						sel_char++;
						if (sel_char == line.get(sel_line).str.length())
							sel_char = -1;
					}
					
					break;
			}
		}
			
		// Force scroll
		int max_lines = (int)floor(h / 24);
		while (sel_line < start_line || sel_line >= (start_line + max_lines)) {
			int dir = (start_line > sel_line ? -1 : 1);
			start_line += dir;
			scroll = start_line;
		}
	}
	
	private void add_line(StringContainer c, int row) {
		line.add(c);
		for (int i = (line.size() - 1); i > row; i--)
			line.set(i, line.get(i - 1));
		line.set(row, c);
	}
	
	public int get_size() {
		return line.size();
	}
	public String get_line(int p) {
		return line.get(p).str;
	}
	
	public void load_data(String[] rows) {
		line = new ArrayList<StringContainer>();
		//line.add(new StringContainer());
		start_line = 0;
		scroll = 0;
		
		sel_line = 0;
		sel_char = -1;
		
		for (int i = 0; i < rows.length; i++) {
			StringContainer c = new StringContainer();
			c.str = rows[i];
			c.update_marker(0);
			line.add(c);
		}
	}
	
	public boolean isBreak() {
		if (pc_nr != null) {
			for (int i = 0; i < pc_nr.length; i++) {
				if (mc.PC == pc_nr[i]) {
					if (line.size() > pc_line[i] && line.get(pc_line[i]).breakpoint) {
						return true;
					}
					break;
				}
			}
		}
		return false;
	}
}



// Characters will be added behind the selected character!
// Position -1 is the last character -> adds characters at the end

private class StringContainer {
	public String str;
	public ArrayList<Marker> marker;
	public boolean breakpoint;
	
	public StringContainer() {
		str = "";
		marker = new ArrayList<Marker>();
		breakpoint = false;
	}
	
	public void write(int pos, String p) {
		if (pos != -1)
			str = str.substring(0, pos) + p + str.substring(pos);
		else
			str = str + p;
	}
	
	public boolean erase(int pos) {
		// ..returns true if tried to erase newline
		if (pos == 0 || pos == -1 && str.length() == 0)
			return true;
		
		if (pos != -1)
			str = str.substring(0, pos - 1) + str.substring(pos);
		else
			str = str.substring(0, str.length() - 1);
		return false;
	}
	
	public void update_marker(int start) {
		// Delete existing following markers
		for (int i = (marker.size() - 1); i >= 0; i--) {
			if (marker.get(i).start >= start || marker.get(i).end >= start)
				marker.remove(i);
		}
		
		// Look for words and check them
		for (int s = start; s < str.length(); s++) {
			if (!strcmp(str.charAt(s), " ")) {
				// First letter?
				if (s == 0 || strcmp(str.charAt(s - 1), " ") || strcmp(str.charAt(s), ";")) {
					if (strcmp(str.charAt(s), ";")) {
						Marker m = new Marker();
						m.start = s;
						m.end = str.length() - 1;
						m.clr = color(155, 155, 155, 100);
						
						marker.add(m);
						break;
					}
					
					int e = s;
					for (; e < str.length(); e++) {
						if (strcmp(str.charAt(e), " ") || strcmp(str.charAt(e), ";")) {
							break;
						}
					}
					e--;
					
					// Check if its a keyword
					String word = str.substring(s, e + 1);
					word = word.toUpperCase();
					boolean found = false;
					
					// Adress?
					if (!found && strcmp(word.charAt(word.length() - 1), ":")) {
						Marker m = new Marker();
						m.start = s;
						m.end = e;
						m.clr = color(255, 0, 0, 100);
						
						found = true;
						marker.add(m);
					}
					
					// Number?
					if (!found && strcmp(word.charAt(0), "#")) {
						Marker m = new Marker();
						m.start = s;
						m.end = e;
						m.clr = color(255, 255, 0, 100);
						
						found = true;
						marker.add(m);
					}
					
					// Keyword?
					if (!found && (strcmp(word, word.toUpperCase()) || strcmp(word, word.toLowerCase()))) {
						for (int i = 0; i < txt_keywords.length; i++) {
							if (strcmp(txt_keywords[i], word)) {
								Marker m = new Marker();
								m.start = s;
								m.end = e;
								m.clr = color(0, 255, 0, 100);
								
								found = true;
								marker.add(m);
								break;
							}
						}
					}
				}
			}
		}
	}
}


private class Marker {
	public int start, end;
	public color clr;
	
	public Marker() {
		start = 0;
		end = 0;
		clr = color(255);
	}
}
