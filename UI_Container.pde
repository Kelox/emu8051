public class UI_Container extends UI_Widget {
  public ArrayList<UI_Widget> child;
  
  
  public UI_Container(float px, float py, float pw, float ph) {
    super(px, py, pw, ph);
    
    child = new ArrayList<UI_Widget>();
  }
  
  public void draw() {
    // Draw child
    translate(x, y);
    
    for (int i = 0; i < child.size(); i++) {
      child.get(i).draw();
    }
    
    translate(-x, -y);
  }
  public void pressed(float px, float py) {
    for (int i = 0; i < child.size(); i++) {
      UI_Widget w = child.get(i);
      if (w.contains(px - x, py - y)) {
        w.pressed(px - x, py - y);
      }
    }
  }
  public void released(float px, float py) {
    for (int i = 0; i < child.size(); i++) {
      UI_Widget w = child.get(i);
      if (w.contains(px - x, py - y)) {
        w.released(px - x, py - y);
      }
    }
  }
  public void clicked(float px, float py) {
    for (int i = 0; i < child.size(); i++) {
      UI_Widget w = child.get(i);
      if (w.contains(px - x, py - y)) {
        w.clicked(px - x, py - y);
      }
    }
  }
  public void dragged(float px, float py, float dx, float dy) {
    for (int i = 0; i < child.size(); i++) {
      UI_Widget w = child.get(i);
      if (w.contains(px - x, py - y)) {
        w.dragged(px - x, py - y, dx, dy);
        break;
      }
    }
  }
  public void keypressed(int k, int kcode) {
    
  }
  
  public void add_child(UI_Widget wid) {
    child.add(wid);
  }
}
