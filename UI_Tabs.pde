public class UI_Tabs extends UI_Widget {
	private ArrayList<String> tab_title;
	private ArrayList<UI_Widget> tab_child;
	private float child_width;
	private int selected;
	
	
	public UI_Tabs(float px, float py, float pw, float ph) {
		super(px, py, pw, ph);
		
		tab_title = new ArrayList<String>();
		tab_child = new ArrayList<UI_Widget>();
		child_width = (w / 5) * 4;
		
		selected = 0;
	}
	
	public void draw() {
		// Draw child
		translate(x, y);
		
		if (selected < tab_title.size()) {
			tab_child.get(selected).draw();
		}
		
		translate(-x, -y);
		
		
		// Draw List
		translate(child_width, 0);
		
		strokeWeight(2);
		stroke(0);
		fill(200, 200, 200);
		rect(x, y, w - child_width, h, 16);
		
		textAlign(CENTER, TOP);
		textSize(24);
		fill(0);
		for (int i = 0; i < tab_title.size(); i++) {
			stext(tab_title.get(i), (w - child_width) / 2, 12 + 24 * i, color(255));
		}
		
		stroke(0, 0, 0, 0);
		fill(255, 255, 125, 75);
		rect(5, 24 * (selected + 1) - 16 + 5, w - child_width - 10, 32 - 10, 16);
		
		
		translate(-child_width, 0);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		if (selected < tab_title.size()) {
			if (px < child_width) {
				UI_Widget w = tab_child.get(selected);
				if (w.contains(px - x, py - y))
					w.released(px - x, py - y);
			}
		}
	}
	public void clicked(float px, float py) {
		if (selected < tab_title.size()) {
			if (px >= child_width) {
				int r = (int)floor((py + 12) / 24) - 1;
				if (r != selected && r >= 0 && r < tab_title.size())
					selected = r;
			} else {
				UI_Widget w = tab_child.get(selected);
				if (w.contains(px - x, py - y))
					w.clicked(px - x, py - y);
			}
		}
	}
	public void dragged(float px, float py, float dx, float dy) {
		if (selected < tab_title.size()) {
			if (px < child_width) {
				UI_Widget w = tab_child.get(selected);
				if (w.contains(px, py))
					w.dragged(px, py, dx, dy);
			}
		}
	}
	public void keypressed(int k, int kcode) {
		
	}
	
	public void add_tab(String pTitle, UI_Widget root) {
		root.x = 0;
		root.y = 0;
		root.w = child_width;
		root.h = h;
		
		tab_title.add(pTitle);
		tab_child.add(root);
	}
}
