public abstract class UI_Widget {
	public float x, y, w, h;
	
	public UI_Widget(float px, float py, float pw, float ph) {
		x = px;
		y = py;
		w = pw;
		h = ph;
	}
	
	public abstract void draw();
	public abstract void pressed(float px, float py);
	public abstract void released(float px, float py);
	public abstract void clicked(float px, float py);
	public abstract void dragged(float px, float py, float dx, float dy);
	public abstract void keypressed(int k, int kcode);
	
  public boolean contains(float px, float py) {
    if (px >= x && px < (x + w) && py >= y && py < (y + h))
      return true;
    
    return false;
  }
	public boolean contains(int px, int py) {
	  return contains((float)px, (float)py);
	}
}
