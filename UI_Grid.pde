public class UI_Grid extends UI_Widget {
	private ArrayList<UI_Widget> child;
	private int columns, rows;
	
	
	public UI_Grid(int pColumns, int pRows, float px, float py, float pw, float ph) {
		super(px, py, pw, ph);
		
		child = new ArrayList<UI_Widget>();
		
		columns = pColumns;
		rows = pRows;
	}
	
	public void draw() {
		// Draw child
		translate(x, y);
		
		for (int i = 0; i < child.size(); i++) {
			child.get(i).draw();
		}
		
		translate(-x, -y);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		for (int i = 0; i < child.size(); i++) {
			UI_Widget w = child.get(i);
			if (w.contains(px - x, py - y)) {
				w.released(px - x, py - y);
			}
		}
	}
	public void clicked(float px, float py) {
		for (int i = 0; i < child.size(); i++) {
			UI_Widget w = child.get(i);
			if (w.contains(px - x, py - y)) {
				w.clicked(px - x, py - y);
			}
		}
	}
	public void dragged(float px, float py, float dx, float dy) {
		for (int i = 0; i < child.size(); i++) {
			UI_Widget w = child.get(i);
			if (w.contains(px - x, py - y)) {
				w.dragged(px - x, py - y, dx, dy);
				break;
			}
		}
	}
	public void keypressed(int k, int kcode) {
		
	}
	
	public void add_child(UI_Widget wid, int w_c, int w_r, int w_w, int w_h) {
		int cellW = (int)(w / (float)columns);
		int cellH = (int)(h / (float)rows);
		
		wid.x = w_c * cellW + 4;
		wid.y = w_r * cellH + 4;
		wid.w = w_w * cellW - 8;
		wid.h = w_h * cellH - 8;
		
		
		child.add(wid);
	}
}
