public class UI_TextField extends UI_Widget {
	private String title;
	public String txt;
	
	// Animation
	private float anim_press;
	
	public UI_TextField(String pTitle, float px, float py, float pw, float ph) {
		super(px, py, pw, ph);
		
		title = pTitle;
		txt = "";
	}
	
	public void draw() {
		// Shadow
		stroke(0, 0, 0, 0);
		fill(50, 50, 50, 50);
		rect(x, y + 5, w, h, 12);
		
		if (anim_press > 0)
			anim_press -= 0.8;
		
		
		// Button
		translate(0, anim_press);
		
		stroke(210, 210, 210);
		strokeWeight(6);
		fill(255);
		rect(x, y, w, h, 12);
		
		textSize(24);
		textAlign(LEFT, CENTER);
		fill(100, 100, 100, 170);
		if (txt.length() == 0)
			text(title, x + 16, y + h / 2);
		else
			stext(txt, x + 16, y + h / 2, color(255));
		
		if (ui_active == this) {
			float tw = textWidth(txt);
			
			strokeWeight(1);
			stroke(0);
			line(x + 16 + tw, y + h * 0.2, x + 16 + tw, y + h * 0.8);
		}
		
		
		translate(0, -anim_press);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		anim_press = 5;
		ui_active = this;
	}
	public void dragged(float px, float py, float dx, float dy) {
		
	}
	public void keypressed(int k, int kcode) {
		//dprint("key " + k + " " + str(char(k)));
		
		boolean allowed = true;
		for (int i = 0; i < txt_disallowed.length; i++) {
			if (txt_disallowed[i] == kcode) {
				allowed = false;
				break;
			}
		}
		
		if (allowed) {
			txt += str(char(k));
		} else {
			switch (kcode) {
				case (TXT_BACKSPACE):
          if (txt.length() > 0)
					  txt = txt.substring(0, txt.length() - 1);
					break;
			}
		}
	}
}
