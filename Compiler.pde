public class Compiler {
	private UI_Editor editor; // get_size(), get_line(line)
	private ArrayList<Command> cmd_list;
	private ArrayList<String> jmp_name;
	private ArrayList<Command> jmp_cmd;
	
	private boolean has_error;
	
	
	public Compiler(UI_Editor pEditor) {
    editor = pEditor;
    has_error = false;
    type_check();
	}
	
	
	public void compile() {
		dprint("Compiling code...", color(255, 255, 0));
		mc.reset();
		
		cmd_list = new ArrayList<Command>();
		has_error = false;
		jmp_name = new ArrayList<String>();
		jmp_cmd = new ArrayList<Command>(); // the command AFTER "name:"
		
		editor.pc_nr = null;
		editor.pc_line = null;
		
		// Load all lines
		for (int i = 0; i < editor.get_size(); i++) {
			String str = editor.get_line(i);
			
			if (str.length() == 0)
				continue;
			
			Command c = new Command();
			c.set_line(str, i + 1);
			if (c.cmd == "")
				continue;
			else if (is_cmd(c.cmd, "JMP") || is_cmd(c.cmd, "AJMP"))
				c.cmd = "LJMP";
			else if (is_cmd(c.cmd, "CALL") || is_cmd(c.cmd, "ACALL"))
				c.cmd = "LCALL";
			
			cmd_list.add(c);
		}
		
		// Compile lines
		for (int i = 0; i < cmd_list.size(); i++) {
			if (has_error) {
				return;
			}
			
			Command c = cmd_list.get(i);
			
			// Entry-Point?
			if (strcmp(c.cmd.charAt(c.cmd.length() - 1), ":")) {
				jmp_name.add(c.cmd.substring(0, c.cmd.length() - 1));
				
				if (c == cmd_list.get(cmd_list.size() - 1)) {
					err("Entry points at nothing!", c.line_number);
				} else {
					// TODO!!! Next cmd MIGHT be another entry-point!
					jmp_cmd.add(cmd_list.get(i + 1));
				}
				
				continue;
			}
			
			// compile
			compile_cmd(c);
		}
		
		// Combine identical Entry-Points
		boolean found = false;
		/*do {
			found = false;
			
			for (int i = 0; i < (jmp_name.size() - 1); i++) {
				String ph = jmp_cmd.get(i).cmd;
				if (ph.charAt(ph.length - 1) == ":") {
					found = true;
					jmp_cmd.get(i) = jmp_cmd.get(i + 1);
				}
			}
		} while (found);*/
		
		// Replace Entry-Point parameters by addresses & compile again
		// (must look for code = { 0, 0, ... })
		for (int i = 0; i < cmd_list.size(); i++) {
			if (has_error)
				return;
			
			Command c = cmd_list.get(i);
			found = false;
			if (c.code != null) {
				found = true;
				for (int k = 0; k < c.code.length; k++) {
					if (c.code[k] != 0) {
						found = false;
						break;
					}
				}
				
				// nop?
				if (c.param == null)
					found = false;
			}
			
			if (found) {
				// replace entry
				boolean replaced = false;
				for (int k = 0; k < c.param.length; k++) {
					for (int v = 0; v < jmp_name.size(); v++) {
						if (strcmp(jmp_name.get(v), c.param[k]) || strcmp(c.param[k].charAt(0), "#") && strcmp(jmp_name.get(v), c.param[k].substring(1))) {
							Command target = jmp_cmd.get(v);
							Command current = cmd_list.get(0);
							int sum = 0;
							int idx = 0;
							while (current != target) {
								if (current.code != null)
									sum += current.code.length;
								idx++;
								current = cmd_list.get(idx);
							}
							
							if (strcmp(c.param[k].charAt(0), "#"))
								c.param[k] = "#" + sum;
							else
								c.param[k] = "" + sum;
							replaced = true;
						}
					}
				}
				
				
				if (!replaced) {
					err("Unknown Entry-Point!", c.line_number);
					return;
				}
				
				// compile
				compile_cmd(c);
			}
		}
		
		
		if (has_error)
			return;
		
		// Set line marker
		int cmd_count = 0;
		for (int i = 0; i < cmd_list.size(); i++) {
			Command c = cmd_list.get(i);
			if (c.code != null) {
				cmd_count++;
			}
		}
		editor.pc_nr = new int[cmd_count];
		editor.pc_line = new int[cmd_count];
		cmd_count = 0;
		int idx = 0;
		for (int i = 0; i < cmd_list.size(); i++) {
			Command c = cmd_list.get(i);
			if (c.code != null) {
				editor.pc_nr[idx] = cmd_count;
				editor.pc_line[idx] = c.line_number - 1;
				idx++;
				cmd_count += c.code.length;
			}
		}
		
		
		// Set values
		int k = 0;
		for (int i = 0; i < cmd_list.size(); i++) {
			Command c = cmd_list.get(i);
			if (c.code != null) {
				for (int v = 0; v < c.code.length; v++) {
					mc.FEPROM[k] = c.code[v];
					k++;
				}
			}
		}
		
		dprint("Compiled successfully!", color(0, 255, 0));
	}
	
	private void compile_cmd(Command c) {
		if (is_cmd(c.cmd, "MOV")) { // Datentransport
			c_mov(c);
		} else if (is_cmd(c.cmd, "POP")) {
			c_pop(c);
		} else if (is_cmd(c.cmd, "PUSH")) {
			c_push(c);
		} else if (is_cmd(c.cmd, "NOP")) {
			c_nop(c);
		} else if (is_cmd(c.cmd, "XCH")) {
			c_xch(c);
		} else if (is_cmd(c.cmd, "MOVC")) {
			c_movc(c);
		} else if (is_cmd(c.cmd, "CLR")) { // Arithmetische Operationen
			c_clr(c);
		} else if (is_cmd(c.cmd, "CPL")) {
			c_cpl(c);
		} else if (is_cmd(c.cmd, "INC")) {
			c_inc(c);
		} else if (is_cmd(c.cmd, "DEC")) {
			c_dec(c);
		} else if (is_cmd(c.cmd, "ADD")) {
			c_add(c);
		} else if (is_cmd(c.cmd, "ADDC")) {
			c_addc(c);
		} else if (is_cmd(c.cmd, "SUBB")) {
			c_subb(c);
		} else if (is_cmd(c.cmd, "SWAP")) {
			c_swap(c);
		} else if (is_cmd(c.cmd, "RL")) {
			c_rl(c);
		} else if (is_cmd(c.cmd, "RR")) {
			c_rr(c);
		} else if (is_cmd(c.cmd, "SETB")) {
			c_setb(c);
		} else if (is_cmd(c.cmd, "ANL")) { // Logische Operationen
			c_anl(c);
		} else if (is_cmd(c.cmd, "ORL")) {
			c_orl(c);
		} else if (is_cmd(c.cmd, "XRL")) {
			c_xrl(c);
    } else if (is_cmd(c.cmd, "LJMP")) { // Sprungbefehle
			c_ljmp(c);
    } else if (is_cmd(c.cmd, "SJMP")) {
      c_sjmp(c);
    } else if (is_cmd(c.cmd, "JBC")) {
			c_jbc(c);
    } else if (is_cmd(c.cmd, "JB")) {
			c_jb(c);
		} else if (is_cmd(c.cmd, "JNB")) {
			c_jnb(c);
		} else if (is_cmd(c.cmd, "JC")) {
			c_jc(c);
		} else if (is_cmd(c.cmd, "JNC")) {
			c_jnc(c);
		} else if (is_cmd(c.cmd, "JZ")) {
			c_jz(c);
		} else if (is_cmd(c.cmd, "JNZ")) {
			c_jnz(c);
		} else if (is_cmd(c.cmd, "DJNZ")) {
			c_djnz(c);
		} else if (is_cmd(c.cmd, "CJNE")) {
			c_cjne(c);
		} else if (is_cmd(c.cmd, "LCALL")) {
			c_lcall(c);
		} else if (is_cmd(c.cmd, "RET")) {
			c_ret(c);
		} else if (is_cmd(c.cmd, "RETI")) {
			c_reti(c);
    } else if (is_cmd(c.cmd, "ORG")) {
      c_org(c);
    } else if (is_cmd(c.cmd, "DB")) {
      c_db(c);
		} else {
			err("Unknown command \"" + c.cmd + "\"!", c.line_number);
		}
	}
	
	
	/////////////////////
	// COMPILE METHODS //
	/////////////////////
	
	private void c_mov(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("MOV expects 2 parameters!", c.line_number);
			return;
		}
    
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x74;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0xE8 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0xE5;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0xE6 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
			case (CMP_REGISTER):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x78 + int(str(c.param[0].charAt(1)));
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_A):
						c.code = new int[1];
						c.code[0] = 0xF8 + int(str(c.param[0].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0xA8 + int(str(c.param[0].charAt(1)));
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
				}
				break;
			case (CMP_DADR):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[3];
						c.code[0] = 0x75;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						c.code[2] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_A):
						c.code = new int[2];
						c.code[0] = 0xF5;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[2];
						c.code[0] = 0x88 + int(str(c.param[1].charAt(1)));
						c.code[1] = read_address_err(c.param[0], c.line_number);
						return;
					case (CMP_DADR):
						c.code = new int[3];
						c.code[0] = 0x85;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						c.code[2] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[2];
						c.code[0] = 0x86 + int(str(c.param[1].charAt(2)));
						c.code[1] = read_address_err(c.param[0], c.line_number);
						return;
				}
				break;
			case (CMP_POINTER):
				switch (get_type(c.param[1])) {
					case (CMP_A):
						c.code = new int[1];
						c.code[0] = 0xF6 + int(str(c.param[0].charAt(2)));
						return;
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x76 + int(str(c.param[0].charAt(2)));
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0xA6 + int(str(c.param[0].charAt(2)));
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
				}
				break;
			case (CMP_BADR):
				switch (get_type(c.param[1])) {
					case (CMP_CARRY):
						c.code = new int[2];
						c.code[0] = 0x92;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						return;
				}
				break;
			case (CMP_CARRY):
				switch (get_type(c.param[1])) {
					case (CMP_BADR):
						c.code = new int[2];
						c.code[0] = 0xA2;
						c.code[1] = read_bitaddress_err(c.param[1], c.line_number);
						return;
				}
				break;
			case (CMP_DPTR):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[3];
						c.code[0] = 0x90;
						c.code[1] = read_constant(c.param[1]);
						if (c.code[1] < 0 || c.code[1] > 65535) {
							err("Expected an 16-Bit value!", c.line_number);
							return;
						}
						
						c.code[2] = c.code[1] & 0b0000000011111111;
						c.code[1] = (c.code[1] >> 8) & 0b0000000011111111;
						return;
          case (CMP_DADR):
            c.code = new int[3];
            c.code[0] = 0x90;
            c.code[1] = read_address(c.param[1]);
            if (c.code[1] < 0 || c.code[1] > 65535) {
              err("Expected an 16-Bit value!", c.line_number);
              return;
            }
            
            c.code[2] = c.code[1] & 0b0000000011111111;
            c.code[1] = (c.code[1] >> 8) & 0b0000000011111111;
            return;
					case (CMP_UNKNOWN):
						c.code = new int[3];
						c.code[0] = 0;
						c.code[1] = 0;
						c.code[2] = 0;
						return;
				}
				break;
		}
		
		err("Unknown MOV combination!", c.line_number);
	}
	
	private void c_nop(Command c) {
		if (c.param != null) {
			err("NOP expects 0 parameters!", c.line_number);
			return;
		}
		
		c.code = new int[1];
		c.code[0] = 0x00;
	}
	
	private void c_pop(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("POP expects 1 parameter!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0xD0;
				c.code[1] = read_address_err(c.param[0], c.line_number);
				return;
		}
		
		err("Unknown POP combination!", c.line_number);
	}
	
	private void c_push(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("PUSH expects 1 parameter!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0xC0;
				c.code[1] = read_address_err(c.param[0], c.line_number);
				return;
		}
		
		err("Unknown PUSH combination!", c.line_number);
	}
	
	private void c_xch(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("XCH expects 2 parameters!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0xC8 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0xC5;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0xC6 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
		}
		
		err("Unknown XCH combination!", c.line_number);
	}
	
	private void c_movc(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("MOVC expects 2 parameters!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_A_DPTR):
						c.code = new int[1];
						c.code[0] = 0x93;
						return;
					case (CMP_A_PC):
						c.code = new int[1];
						c.code[0] = 0x83;
						return;
				}
				break;
		}
		
		err("Unknown MOVC combination!", c.line_number);
	}
	
	
	// ARITHMETISCHE OPERATIONEN //
	
	private void c_clr(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("CLR expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0xE4;
				return;
			case (CMP_CARRY):
				c.code = new int[1];
				c.code[0] = 0xC3;
				return;
			case (CMP_BADR):
				c.code = new int[2];
				c.code[0] = 0xC2;
				c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
				return;
		}
		
		err("Unknown CLR combination!", c.line_number);
	}
	
	private void c_cpl(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("CPL expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0xF4;
				return;
			case (CMP_CARRY):
				c.code = new int[1];
				c.code[0] = 0xB3;
				return;
			case (CMP_BADR):
				c.code = new int[2];
				c.code[0] = 0xB2;
				c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
				return;
		}
		
		err("Unknown CPL combination!", c.line_number);
	}
	
	private void c_inc(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("INC expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0x04;
				return;
			case (CMP_REGISTER):
				c.code = new int[1];
				c.code[0] = 0x08 + int(str(c.param[0].charAt(1)));
				return;
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0x05;
				c.code[1] = read_address_err(c.param[0], c.line_number);
				return;
			case (CMP_DPTR):
				c.code = new int[1];
				c.code[0] = 0xA3;
				return;
			case (CMP_POINTER):
				c.code = new int[1];
				c.code[0] = 0x06 + int(str(c.param[0].charAt(2)));
				return;
		}
		
		err("Unknown INC combination!", c.line_number);
	}
	
	private void c_dec(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("DEC expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0x14;
				return;
			case (CMP_REGISTER):
				c.code = new int[1];
				c.code[0] = 0x18 + int(str(c.param[0].charAt(1)));
				return;
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0x15;
				c.code[1] = read_address_err(c.param[0], c.line_number);
				return;
			case (CMP_POINTER):
				c.code = new int[1];
				c.code[0] = 0x16 + int(str(c.param[0].charAt(2)));
				return;
		}
		
		err("Unknown DEC combination!", c.line_number);
	}
	
	private void c_add(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("ADD expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x24;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0x28 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0x25;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0x26 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
		}
		
		err("Unknown ADD combination!", c.line_number);
	}
	
	private void c_addc(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("ADDC expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x34;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0x38 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0x35;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0x36 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
		}
		
		err("Unknown ADDC combination!", c.line_number);
	}
	
	private void c_subb(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("SUBB expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x94;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0x98 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0x95;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0x96 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
		}
		
		err("Unknown SUBB combination!", c.line_number);
	}
	
	private void c_swap(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("SWAP expects 1 parameter!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0xC4;
				return;
		}
		
		err("Unknown SWAP combination!", c.line_number);
	}
	
	private void c_rl(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("RL expects 1 parameter!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0x23;
				return;
		}
		
		err("Unknown RL combination!", c.line_number);
	}
	
	private void c_rr(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("RR expects 1 parameter!", c.line_number);
      return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				c.code = new int[1];
				c.code[0] = 0x03;
				return;
		}
		
		err("Unknown RR combination!", c.line_number);
	}
	
	private void c_setb(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("SETB expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_CARRY):
				c.code = new int[1];
				c.code[0] = 0xD3;
				return;
			case (CMP_BADR):
				c.code = new int[2];
				c.code[0] = 0xD2;
				c.code[1] = read_bitaddress(c.param[0]);
				if (c.code[1] == -1) {
					err("Could not read bit-address!", c.line_number);
				}
				return;
		}
		
		err("Unknown SETB combination!", c.line_number);
	}
	
	
	// LOGISCHE OPERATIONEN //
	
	private void c_anl(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("ANL expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x54;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0x58 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0x55;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0x56 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
			case (CMP_DADR):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[3];
						c.code[0] = 0x53;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						c.code[2] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_A):
						c.code = new int[2];
						c.code[0] = 0x52;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						return;
				}
				break;
			case (CMP_CARRY):
				switch (get_type(c.param[1])) {
					case (CMP_BADR):
						c.code = new int[2];
						c.code[0] = 0x82;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						return;
					case (CMP_INV_BADR):
						c.code = new int[2];
						c.code[0] = 0xB0;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						return;
				}
				break;
		}
		
		err("Unknown ANL combination!", c.line_number);
	}
	
	private void c_orl(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("ORL expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x44;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0x48 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0x45;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0x46 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
			case (CMP_DADR):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[3];
						c.code[0] = 0x43;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						c.code[2] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_A):
						c.code = new int[2];
						c.code[0] = 0x42;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						return;
				}
				break;
			case (CMP_CARRY):
				switch (get_type(c.param[1])) {
					case (CMP_BADR):
						c.code = new int[2];
						c.code[0] = 0x72;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						return;
					case (CMP_INV_BADR):
						c.code = new int[2];
						c.code[0] = 0xA0;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						return;
				}
				break;
		}
		
		err("Unknown ORL combination!", c.line_number);
	}
	
	private void c_xrl(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("XRL expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[2];
						c.code[0] = 0x64;
						c.code[1] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_REGISTER):
						c.code = new int[1];
						c.code[0] = 0x68 + int(str(c.param[1].charAt(1)));
						return;
					case (CMP_DADR):
						c.code = new int[2];
						c.code[0] = 0x65;
						c.code[1] = read_address_err(c.param[1], c.line_number);
						return;
					case (CMP_POINTER):
						c.code = new int[1];
						c.code[0] = 0x66 + int(str(c.param[1].charAt(2)));
						return;
				}
				break;
			case (CMP_DADR):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code = new int[3];
						c.code[0] = 0x63;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						c.code[2] = read_constant_err(c.param[1], c.line_number);
						return;
					case (CMP_A):
						c.code = new int[2];
						c.code[0] = 0x62;
						c.code[1] = read_address_err(c.param[0], c.line_number);
						return;
				}
				break;
		}
		
		err("Unknown XRL combination!", c.line_number);
	}
	
	
	// SPRUNGBEFEHLE //
	
	private void c_ljmp(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("LJMP expects 1 parameter!", c.line_number);
			return;
		}
    
		int type = get_type(c.param[0]);
		if (type == CMP_UNKNOWN) {
			// Entry-Point: recompile with address later
			c.code = new int[3];
			c.code[0] = 0;
			c.code[1] = 0;
			c.code[2] = 0;
			return;
		}
		
		int num = read_address(c.param[0]);
		if (num == -1) {
			err("Could not read address!", c.line_number);
			return;
		} else if (num < 0 || num > 65535) {
			err("Expected an 16-Bit value!", c.line_number);
			return;
		}
		
		c.code = new int[3];
		c.code[0] = 0x02;
		c.code[1] = (num & 0b1111111100000000) >> 8;
		c.code[2] = num & 0b0000000011111111;
	}
	
	private void c_sjmp(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("SJMP expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0x80;
				c.code[1] = read_address_rel(c.param[0], c, c.line_number);
				return;
		}
		
		err("Unknown SJMP combination!", c.line_number);
	}
	
	private void c_jbc(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("JBC expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_BADR):
				switch (get_type(c.param[1])) {
					case (CMP_DADR):
						c.code = new int[3];
						c.code[0] = 0x10;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						c.code[2] = read_address_rel(c.param[1], c, c.line_number);
						return;
					case (CMP_UNKNOWN):
						c.code = new int[3];
						c.code[0] = 0;
						c.code[1] = 0;
						c.code[2] = 0;
						return;
				}
				break;
		}
		
		err("Unknown JBC combination!", c.line_number);
	}
	
	private void c_jb(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("JB expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_BADR):
				switch (get_type(c.param[1])) {
					case (CMP_DADR):
						c.code = new int[3];
						c.code[0] = 0x20;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						c.code[2] = read_address_rel(c.param[1], c, c.line_number);
						return;
					case (CMP_UNKNOWN):
						c.code = new int[3];
						c.code[0] = 0;
						c.code[1] = 0;
						c.code[2] = 0;
						return;
				}
				break;
		}
		
		err("Unknown JB combination!", c.line_number);
	}
	
	private void c_jnb(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("JNB expects 2 parameters!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_BADR):
				switch (get_type(c.param[1])) {
					case (CMP_DADR):
						c.code = new int[3];
						c.code[0] = 0x30;
						c.code[1] = read_bitaddress_err(c.param[0], c.line_number);
						c.code[2] = read_address_rel(c.param[1], c, c.line_number);
						return;
					case (CMP_UNKNOWN):
						c.code = new int[3];
						c.code[0] = 0;
						c.code[1] = 0;
						c.code[2] = 0;
						return;
				}
				break;
		}
		
		err("Unknown JNB combination!", c.line_number);
	}
	
	private void c_jc(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("JC expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0x40;
				c.code[1] = read_address_rel(c.param[0], c, c.line_number);
				return;
			case (CMP_UNKNOWN):
				c.code = new int[2];
				c.code[0] = 0;
				c.code[1] = 0;
				return;
		}
		
		err("Unknown JC combination!", c.line_number);
	}
	
	private void c_jnc(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("JNC expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_DADR):
				c.code = new int[2];
				c.code[0] = 0x50;
				c.code[1] = read_address_rel(c.param[0], c, c.line_number);
				return;
			case (CMP_UNKNOWN):
				c.code = new int[2];
				c.code[0] = 0;
				c.code[1] = 0;
				return;
		}
		
		err("Unknown JNC combination!", c.line_number);
	}
	
	private void c_jz(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("JZ expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_UNKNOWN):
				c.code = new int[2];
				c.code[0] = 0;
				c.code[1] = 0;
				return;
			case (CMP_DADR):
				int rel = read_address(c.param[0]);
				if (rel >= 0 && rel < 65536) {
					rel = rel - (size_to(c) + 2);
					if (rel >= -128 && rel <= 127) {
						c.code = new int[2];
						c.code[0] = 0x60;
						c.code[1] = rel;
					} else {
						err("Jump out of range!", c.line_number);
					}
				} else {
					err("Could not read address!", c.line_number);
				}
				return;
		}
		
		err("Unknown JNZ combination!", c.line_number);
	}
	
	private void c_jnz(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("JNZ expects 1 parameter!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_UNKNOWN):
				c.code = new int[2];
				c.code[0] = 0;
				c.code[1] = 0;
				return;
			case (CMP_DADR):
				int rel = read_address(c.param[0]);
				if (rel >= 0 && rel < 65536) {
					rel = rel - (size_to(c) + 2);
					if (rel >= -128 && rel <= 127) {
						c.code = new int[2];
						c.code[0] = 0x70;
						c.code[1] = rel;
					} else {
						err("Jump out of range!", c.line_number);
					}
				} else {
					err("Could not read address!", c.line_number);
				}
				return;
		}
		
		err("Unknown JNZ combination!", c.line_number);
	}
	
	private void c_djnz(Command c) {
		if (c.param == null || c.param.length != 2) {
			err("DJNZ expects 2 parameters!", c.line_number);
			return;
		}
		
		int rel;
		
		switch (get_type(c.param[0])) {
			case (CMP_REGISTER): // DJNZ Rn, rel
				switch (get_type(c.param[1])) {
					case (CMP_DADR): // rel is an address
						rel = read_address(c.param[1]);
						if (rel >= 0 && rel < 65536) {
							rel = rel - (size_to(c) + 2);
							if (rel >= -128 && rel <= 127) {
								c.code = new int[2];
								c.code[0] = 0xD8 + int(str(c.param[0].charAt(1)));
								c.code[1] = rel;
							} else {
								err("Jump out of range!", c.line_number);
							}
						} else {
							err("Could not read address!", c.line_number);
						}
						return;
					case (CMP_UNKNOWN): // rel is an entry-point
						c.code = new int[2];
						c.code[0] = 0;
						c.code[1] = 0;
						return;
				}
				break;
			case (CMP_DADR): // DJNZ dadr, rel
				switch (get_type(c.param[1])) {
					case (CMP_DADR): // rel is an address
						rel = read_address(c.param[1]);
						if (rel >= 0 && rel < 65536) {
							rel = rel - (size_to(c) + 3);
							if (rel >= -128 && rel <= 127) {
								c.code = new int[3];
								c.code[0] = 0xD5;
								c.code[1] = read_address(c.param[0]);
								c.code[2] = rel;
							} else {
								err("Jump out of range!", c.line_number);
							}
						} else {
							err("Could not read address!", c.line_number);
						}
						return;
					case (CMP_UNKNOWN): // rel is an entry-point
						c.code = new int[3];
						c.code[0] = 0;
						c.code[1] = 0;
						c.code[2] = 0;
						return;
				}
				break;
		}
		
		err("Unknown DJNZ combination!", c.line_number);
	}
	
	private void c_cjne(Command c) {
		if (c.param == null || c.param.length != 3) {
			err("CJNE expects 3 parameters!", c.line_number);
			return;
		}
		
		if (get_type(c.param[2]) == CMP_UNKNOWN) {
			c.code = new int[3];
			for (int i = 0; i < c.code.length; i++)
				c.code[i] = 0;
			return;
		}
		
		int rel = read_address(c.param[2]);
		if (rel >= 0 && rel < 65536) {
			rel = rel - (size_to(c) + 3);
			if (rel >= -128 && rel <= 127) {
				c.code = new int[3];
				c.code[2] = rel;
			} else {
				err("Jump out of range!", c.line_number);
				return;
			}
		} else {
			err("Could not read address!", c.line_number);
			return;
		}
		
		switch (get_type(c.param[0])) {
			case (CMP_A):
				switch (get_type(c.param[1])) {
					case (CMP_CONST): // CJNE A, #c8, rel
						c.code[0] = 0xB4;
						c.code[1] = read_constant(c.param[1]);
						return;
					case (CMP_DADR): // CJNE A, dadr, rel
						c.code[0] = 0xB5;
						c.code[1] = read_address(c.param[1]);
						return;
				}
				break;
			case (CMP_REGISTER):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code[0] = 0xB8 + int(str(c.param[0].charAt(1)));
						c.code[1] = read_constant(c.param[1]);
						return;
				}
				break;
			case (CMP_POINTER):
				switch (get_type(c.param[1])) {
					case (CMP_CONST):
						c.code[0] = 0xB6 + int(str(c.param[0].charAt(2)));
						c.code[1] = read_constant(c.param[1]);
						return;
				}
				break;
		}
		
		err("Unknown CJNE combination!", c.line_number);
	}
	
	private void c_lcall(Command c) {
		if (c.param == null || c.param.length != 1) {
			err("LCALL expects 1 parameter!", c.line_number);
			return;
		}
		
		int type = get_type(c.param[0]);
		if (type == CMP_UNKNOWN) {
			// Entry-Point: recompile with address later
			c.code = new int[3];
			c.code[0] = 0;
			c.code[1] = 0;
			c.code[2] = 0;
			return;
		}
		
		int num = read_address(c.param[0]);
		if (num == -1) {
			err("Could not read address!", c.line_number);
			return;
		} else if (num < 0 || num > 65535) {
			err("Expected an 16-Bit value!", c.line_number);
			return;
		}
		
		c.code = new int[3];
		c.code[0] = 0x12;
    c.code[1] = (num & 0b1111111100000000) >> 8;
		c.code[2] = num & 0b0000000011111111;
	}
	
	private void c_ret(Command c) {
		if (c.param != null) {
			err("RET expects 0 parameters!", c.line_number);
			return;
		}
		
		c.code = new int[1];
		c.code[0] = 0x22;
	}
	
	private void c_reti(Command c) {
		if (c.param != null) {
			err("RET expects 0 parameters!", c.line_number);
			return;
		}
		
		c.code = new int[1];
		c.code[0] = 0x32;
	}
	
  
  // SONSTIGE //
  
  private void c_org(Command c) {
    if (c.param == null || c.param.length != 1) {
      err("ORG expects 1 parameter!", c.line_number);
      return;
    }
    
    if (get_type(c.param[0]) == CMP_DADR) {
      int start_adr = read_address(c.param[0]);
      int current_adr = 0;
      
      for (int i = 0; i < cmd_list.size(); i++) {
        if (cmd_list.get(i) == c)
          break;
        
        if (cmd_list.get(i).code != null)
          current_adr += cmd_list.get(i).code.length;
      }
      
      if (current_adr > start_adr) {
        err("Memory at " + c.param[0] + " already occupied!", c.line_number);
        return;
      } else if (current_adr < start_adr) {
        c.code = new int[start_adr - current_adr];
        for (int i = 0; i < c.code.length; i++)
          c.code[i] = 0xFF;
      } else {
        c.code = null;
      }
      return;
    }
    
    err("Unknown ORG combination!", c.line_number);
  }
  
  private void c_db(Command c) {
    if (c.param == null || c.param.length < 1) {
      err("DB expects at least 1 parameter!", c.line_number);
      return;
    }
    
    c.code = new int[c.param.length];
    for (int i = 0; i < c.param.length; i++) {
      if (get_type(c.param[i]) != CMP_DADR) {
        err("Unknown DB combination!", c.line_number);
        return;
      }
      
      c.code[i] = read_address(c.param[i]);
    }
  }
	
	
	
	
	////////////////////
	// USEFUL METHODS //
	////////////////////
	
	public int get_type(String pStr) {
		if (pStr.length() == 0)
			return CMP_UNKNOWN;
		
		// Akku?
		if (strcmp(pStr, "A")) {
			return CMP_A;
		} else if (strcmp(pStr, "C")) {
			return CMP_CARRY;
		} else if (strcmp(pStr, "DPTR")) {
			return CMP_DPTR;
		} else if (pStr.length() == 2 && strcmp(pStr.charAt(0), "R") && strcmp("" + int(str(pStr.charAt(1))), pStr.charAt(1)) && int(str(pStr.charAt(1))) < 8) {
			return CMP_REGISTER;
		} else if (strcmp(pStr.charAt(0), "#")) {
			if (get_type(pStr.substring(1)) == CMP_DADR)
				return CMP_CONST;
			else
				return CMP_UNKNOWN;
		} else if (strcmp(pStr, "@R0") || strcmp(pStr, "@R1")) {
			return CMP_POINTER;
		} else if (strcmp(pStr, "@A+DPTR")) {
			return CMP_A_DPTR;
		} else if (strcmp(pStr, "@A+PC")) {
			return CMP_A_PC;
		} else if (strcmp(pStr.charAt(0), "/") && get_type(pStr.substring(1)) == CMP_BADR) {
			return CMP_INV_BADR;
		} else if (pStr.length() > 2 && strcmp(pStr.charAt(pStr.length() - 2), ".")) {
			int dot_cnt = 0;
			for (int i = 0; i < pStr.length(); i++) {
				if (strcmp(pStr.charAt(i), "."))
					dot_cnt++;
			}
			
			int bit = int(str(pStr.charAt(pStr.length() - 1)));
			
			if (dot_cnt == 1 && strcmp("" + bit, pStr.charAt(pStr.length() - 1)) && bit >= 0 && bit < 8) {
				String sc = pStr.substring(0, pStr.length() - 2);
				for (int i = 0; i < cmp_shortcut.length; i++) {
					if (strcmp(sc, cmp_shortcut[i])) {
						int adr = ((i % 2) == 0 ? read_address(cmp_shortcut[i]) : read_address(cmp_shortcut[i+1]));
						if ((adr % 8) == 0)
							return CMP_BADR;
						
						// Else not bit-addressable
						break;
					}
				}
			}
			err("Invalid Bit-Address (" + pStr + ")", -1);
			return CMP_UNKNOWN;
		} else {
			// Shortcut?
			for (int i = 0; i < cmp_shortcut.length; i += 2) {
				if (strcmp(cmp_shortcut[i], pStr)) {
					return CMP_DADR;
				}
			}
			
			// Bit-Shortcut?
			for (int i = 0; i < cmp_bit_shortcut.length; i += 2) {
				if (strcmp(cmp_bit_shortcut[i], pStr)) {
					return CMP_BADR;
				}
			}
			
			// DADR?
			String dadr_allowed = "0123456789ABCDEFbdh";
			boolean is_allowed = true;
			// ..check if it only contains legal letters
			for (int i = 0; i < pStr.length(); i++) {
				boolean ph_allowed = false;
				for (int k = 0; k < dadr_allowed.length(); k++) {
					if (strcmp(pStr.charAt(i), dadr_allowed.charAt(k))) {
						ph_allowed = true;
						break;
					}
				}
				
				is_allowed = ph_allowed;
				if (!is_allowed)
					break;
			}
			
			// ..if so..
			if (is_allowed) {
				is_allowed = false;
				
				// ..get the amount of "type-letters" given
				int type_count = 0;
				for (int i = 0; i < pStr.length(); i++) {
					char ph_char = pStr.charAt(i);
					if (strcmp(ph_char, "b") || strcmp(ph_char, "d") || strcmp(ph_char, "h")) {
						type_count++;
					}
				}
				
				// ..if one or none..
				if (type_count <= 1) {
					// ..check if the "number-letters" are all allowed (-> get max index)
					int ph_end = pStr.length();
					int max_dadr = 10; // max. 10th letter of dadr_allowed -> dez
					if (type_count == 1) {
						ph_end--;
						
						char ph_char = pStr.charAt(pStr.length() - 1);
						switch (ph_char) {
							case ('b'):
								max_dadr = 2;
								break;
							case ('d'):
								max_dadr = 10;
								break;
							case ('h'):
								max_dadr = 16;
								break;
						}
					}
					
					// ..check if all letters are inside the range
					int inx = 0; //.. the max. reached index
					for (int i = 0; i < ph_end; i++) {
						for (int k = 0; k < dadr_allowed.length(); k++) {
							if (strcmp(pStr.charAt(i), dadr_allowed.charAt(k))) {
								if (k > inx)
									inx = k;
								break;
							}
						}
					}
					if (inx < max_dadr) {
						// Its an address
						return CMP_DADR;
					}
				}
			}
			
			// BADR?
			for (int i = 0; i < pStr.length(); i++) {
				if (strcmp(pStr.charAt(i), ".")) {
					String phStr = pStr.substring(0, i);
					if (get_type(phStr) == CMP_DADR)
						return CMP_BADR;
					
					break;
				}
			}
		}
		
		// ..possibly a custom address
		return CMP_UNKNOWN;
	}
	
	private boolean is_cmd(String a, String b) {
		if (strcmp(a, b) || strcmp(a.toUpperCase(), b))
			return true;
		
		return false;
	}
	
	private int read_address(String str) {
		// Shortcut?
		for (int i = 0; i < cmp_shortcut.length; i += 2) {
			if (strcmp(cmp_shortcut[i], str)) {
				return read_address(cmp_shortcut[i+1]);
			}
		}
		
		char ph = str.charAt(str.length() - 1);
		
		
		if (strcmp(ph, "d") || (!strcmp(ph, "b") && !strcmp(ph, "h"))) {
			// Decimal
			if (strcmp(ph, "d"))
				str = str.substring(0, str.length() - 1);
			
			String allowed = "0123456789";
			for (int i = 0; i < str.length(); i++) {
				boolean found = false;
				for (int k = 0; k < allowed.length(); k++) {
					if (strcmp(str.charAt(i), allowed.charAt(k))) {
						found = true;
						break;
					}
				}
				
				if (!found)
					return -1;
			}
			
			return int(str);
		} else if (strcmp(ph, "b")) {
			// Binary
			str = str.substring(0, str.length() - 1);
			
			String allowed = "01";
			for (int i = 0; i < str.length(); i++) {
				boolean found = false;
				for (int k = 0; k < allowed.length(); k++) {
					if (strcmp(str.charAt(i), allowed.charAt(k))) {
						found = true;
						break;
					}
				}
				
				if (!found)
					return -1;
			}
			
			return int(unbinary(str));
		} else if (strcmp(ph, "h")) {
			// Hexadecimal
			str = str.substring(0, str.length() - 1);
			
			String allowed = "0123456789ABCDEF";
			for (int i = 0; i < str.length(); i++) {
				boolean found = false;
				for (int k = 0; k < allowed.length(); k++) {
					if (strcmp(str.charAt(i), allowed.charAt(k))) {
						found = true;
						break;
					}
				}
				
				if (!found)
					return -1;
			}
			
			return int(unhex(str));
		}
		
		return -1;
	}
	
	private int read_constant(String str) {
		return read_address(str.substring(1));
	}
	
	private int read_bitaddress(String pStr) {
		// Read bit-shortcut
		for (int i = 0; i < cmp_bit_shortcut.length; i += 2) {
			if (strcmp(pStr, cmp_bit_shortcut[i])) {
				return read_bitaddress(cmp_bit_shortcut[i + 1]);
			}
		}
		
		String dadr_str = pStr.substring(0, pStr.length() - 2);
		for (int i = 0; i < cmp_shortcut.length; i++) {
			if (strcmp(cmp_shortcut[i], dadr_str)) {
				int dadr = read_address(dadr_str);
				int bit = int(str(pStr.charAt(pStr.length() - 1)));
				if (strcmp(bit + "", pStr.charAt(pStr.length() - 1)) && bit >= 0 && bit < 8) {
					return dadr + bit;
				}
				break;
			}
		}
		
		return -1;
	}
	
	private int read_address_err(String str, int line) {
		int ph = read_address(str);
		if (ph < 0 || ph > 255)
			err("Could not read address!", line);
		return ph;
	}
	private int read_address_rel(String str, Command c, int line) {
		int rel = read_address(str);
		if (rel >= 0 && rel < 65536) {
			rel = rel - (size_to(c) + c.code.length);
			if (rel >= -128 && rel <= 127) {
				return rel;
			} else {
				err("Jump out of range!", line);
			}
		} else {
			err("Could not read address!", line);
		}
		
		return -1;
	}
	private int read_constant_err(String str, int line) {
		int ph = read_constant(str);
		if (ph < 0 || ph > 255)
			err("Could not read constant!", line);
		return ph;
	}
	private int read_bitaddress_err(String str, int line) {
		int ph = read_bitaddress(str);
		if (ph == -1)
			err("Could not read bit-address!", line);
		return ph;
	}
	
	private void err(String pStr, int pLine) {
		dprint("[ERROR] (Line " + pLine + ") " + pStr, color(255, 0, 0));
		has_error = true;
	}
	
	private int size_to(Command c) {
		int sum = 0;
		for (int i = 0; i < cmd_list.size(); i++) {
			if (cmd_list.get(i) == c)
				break;
			if (cmd_list.get(i).code != null)
				sum += cmd_list.get(i).code.length;
		}
		return sum;
	}
  
  private void type_check() {
    /*
    final int CMP_UNKNOWN = -1;
    final int CMP_A = 0;
    final int CMP_DADR = 1;
    final int CMP_BADR = 2;
    final int CMP_CONST = 3;
    final int CMP_DPTR = 4;
    final int CMP_SFR = 5; // address inside sfr <--- UNUSED!!
    final int CMP_SFRB = 6; // bit-address inside sfr <--- UNUSED!!
    final int CMP_REGISTER = 7;
    final int CMP_POINTER = 8; // @Rn
    final int CMP_CARRY = 9;
    final int CMP_A_DPTR = 10;
    final int CMP_A_PC = 11;
    final int CMP_INV_BADR = 12;
    */
    if (get_type("A") != CMP_A)              { dprint("type_check failed! ( A )",         color(255, 0, 0)); }
    
    if (get_type("0") != CMP_DADR)           { dprint("type_check failed! ( 0 )",         color(255, 0, 0)); }
    if (get_type("01") != CMP_DADR)          { dprint("type_check failed! ( 01 )",        color(255, 0, 0)); }
    if (get_type("012") != CMP_DADR)         { dprint("type_check failed! ( 012 )",       color(255, 0, 0)); }
    if (get_type("012d") != CMP_DADR)        { dprint("type_check failed! ( 012d )",      color(255, 0, 0)); }
    if (get_type("012h") != CMP_DADR)        { dprint("type_check failed! ( 012h )",      color(255, 0, 0)); }
    
    if (get_type("P2.2") != CMP_BADR)        { dprint("type_check failed! ( P2.2 )",      color(255, 0, 0)); }
    
    if (get_type("#05h") != CMP_CONST)       { dprint("type_check failed! ( #05h )",      color(255, 0, 0)); }
    if (get_type("#05d") != CMP_CONST)       { dprint("type_check failed! ( #05d )",      color(255, 0, 0)); }
    if (get_type("#01b") != CMP_CONST)       { dprint("type_check failed! ( #01b )",      color(255, 0, 0)); }
    if (get_type("#05") != CMP_CONST)        { dprint("type_check failed! ( #05 )",       color(255, 0, 0)); }
    
    if (get_type("DPTR") != CMP_DPTR)        { dprint("type_check failed! ( DPTR )",      color(255, 0, 0)); }
    
    if (get_type("R0") != CMP_REGISTER)      { dprint("type_check failed! ( R0 )",        color(255, 0, 0)); }
    if (get_type("R1") != CMP_REGISTER)      { dprint("type_check failed! ( R1 )",        color(255, 0, 0)); }
    if (get_type("R2") != CMP_REGISTER)      { dprint("type_check failed! ( R2 )",        color(255, 0, 0)); }
    if (get_type("R3") != CMP_REGISTER)      { dprint("type_check failed! ( R3 )",        color(255, 0, 0)); }
    if (get_type("R4") != CMP_REGISTER)      { dprint("type_check failed! ( R4 )",        color(255, 0, 0)); }
    if (get_type("R5") != CMP_REGISTER)      { dprint("type_check failed! ( R5 )",        color(255, 0, 0)); }
    if (get_type("R6") != CMP_REGISTER)      { dprint("type_check failed! ( R6 )",        color(255, 0, 0)); }
    if (get_type("R7") != CMP_REGISTER)      { dprint("type_check failed! ( R7 )",        color(255, 0, 0)); }
    
    if (get_type("@R0") != CMP_POINTER)      { dprint("type_check failed! ( @R0 )",       color(255, 0, 0)); }
    if (get_type("@R1") != CMP_POINTER)      { dprint("type_check failed! ( @R1 )",       color(255, 0, 0)); }
    
    if (get_type("C") != CMP_CARRY)          { dprint("type_check failed! ( C )",         color(255, 0, 0)); }
    
    if (get_type("@A+DPTR") != CMP_A_DPTR)   { dprint("type_check failed! ( @A+DPTR )",   color(255, 0, 0)); }
    
    if (get_type("@A+PC") != CMP_A_PC)       { dprint("type_check failed! ( @A+PC )",     color(255, 0, 0)); }
    
    if (get_type("/P2.2") != CMP_INV_BADR)   { dprint("type_check failed! ( /P2.2 )",     color(255, 0, 0)); }
  }
}


private class Command {
	public String cmd;
	public String[] param;
	public int[] code;
	
	public int line_number;
	
	public Command() {
		cmd = "";
		param = null;
		code = null;
		line_number = -1;
	}
	
	public void set_line(String line, int pNum) {
		// Remove comment
		for (int i = 0; i < line.length(); i++) {
			if (strcmp(line.charAt(i), ";")) {
				line = line.substring(0, i);
				break;
			}
		}
		
		// Remove tabs
		for (int i = 0; i < line.length(); i++) {
			if (!strcmp(line.charAt(i), " ")) {
				if (i != 0)
					line = line.substring(i);
				break;
			} else if (i == (line.length() - 1)) {
				return; // its empty
			}
		}
		
		// Empty?
		if (line.length() == 0)
			return;
		
		
		line_number = pNum;
		
		// Get cmd
		int cmd_end = 0;
		for (; cmd_end < line.length(); cmd_end++) {
			if (strcmp(line.charAt(cmd_end), " "))
				break;
		}
		cmd_end--;
		cmd = line.substring(0, cmd_end + 1);
		
		// Get parameters
		line = line.substring(cmd_end + 1);
		if (line.length() != 0) {
			// Remove whitespace chars
			boolean found = true;
			while (found) {
				found = false;
				
				for (int start = 0; start < line.length(); start++) {
					if (strcmp(line.charAt(start), " ")) {
						found = true;
						int end;
						for (end = start; end < line.length(); end++) {
							if (!strcmp(line.charAt(end), " "))
								break;
						}
						
						line = line.substring(0, start) + line.substring(end);
					}
				}
			}
			
			// Get params
			if (line.length() > 0) {
				param = split(line, ",");
			}
		}
	}
}
