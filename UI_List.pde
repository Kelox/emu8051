public class UI_List extends UI_Widget {
	private int selected;
	private ArrayList<String> item;
	
	private float scroll;
	
	
	public UI_List(int px, int py, int pw, int ph) {
		super(px, py, pw, ph);
		
		selected = 0;
		item = new ArrayList<String>();
		
		scroll = 0;
	}
	
	
	public void draw() {
		translate(x, y);
		
		strokeWeight(1);
		stroke(1);
		fill(225, 225, 225, 255);
		rect(0, 0, w, h, 16);
		
		textSize(24);
		fill(0);
		textAlign(LEFT, TOP);
		
		int start = (int)floor(scroll);
		float sel_pos = -1;
		for (int i = start; i < item.size(); i++) {
			if ((24 * (i - start)) < h && i >= 0 && i < item.size()) {
				text(item.get(i), 0, 24 * (i - start));
				
				if (i == selected)
					sel_pos = 24 * (i - start);
			}
		}
		
		if (sel_pos >= 0) {
			strokeWeight(1);
			stroke(0, 0, 0, 0);
			fill(255, 255, 0, 100);
			rect(0, sel_pos, w, 24, 16);
		}
		
		// Scroll bar
		float m = scroll / (float)(item.size() - 1);
		stroke(0, 0, 0, 0);
		fill(177);
		rect(w - 8, (h / 5) * 4 * m, 4, h / 5, 2);
		
		translate(-x, -y);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		selected = (int)floor((py - y) / 24) + floor(scroll);
	}
	public void dragged(float px, float py, float dx, float dy) {
		scroll -= dy / 24;
		scroll = (scroll < 0 ? 0 : scroll);
		scroll = (scroll > (item.size() - 1) ? (item.size() - 1) : scroll);
	}
	public void keypressed(int k, int kcode) {
		
	}
	
	public String get_selected() {
		if (selected >= 0 && selected < item.size()) {
			return item.get(selected);
		}
		return "";
	}
	public void add_item(String str) {
		item.add(str);
	}
	public void remove_item(String str) {
		for (int i = 0; i < item.size(); i++) {
			if (item.get(i) == str) {
				item.remove(i);
				break;
			}
		}
		selected = -1;
	}
}
