public class GUI_Controller {
	private UI_Tabs root_tabs;
	
	UI_Editor editor;
	
	// Tools
	UI_Button btn_dprint;
	UI_Scale speed_scale;
	
  UI_Button btn_markpc;
  
	UI_Button btn_compile;
	UI_Button btn_reset;
	UI_Button btn_run;
  UI_Button btn_clear;
  UI_Button btn_clear_debug;
	boolean btn_run_last_state;
	float run_timer = 0;
	
	UI_Button btn_quick_access;
	UI_Button btn_info;
	
	UI_FloatingWindow fw_quick;
	UI_Button fwq_step;
	UI_Button fwq_run;
	UI_Button fwq_reset;
	UI_Button fwq_compile;
	boolean fw_quick_last_state;
	boolean fwq_run_last_state;
	
	UI_FloatingWindow fw_info;
	UI_Text fwi_txt;
	boolean fw_info_last_state;
	
	// Data
	UI_TextField tf_filename;
	UI_Button btn_save;
	UI_List file_list;
	UI_Button btn_load;
	UI_Button btn_overwrite;
	UI_Button btn_delete;
	
	// Views
	UI_Data view_feprom;
	UI_Data view_ram;
	UI_Data view_sfr;
  
  // Custom registers
  UI_Container cst_cont;
  UI_TextField cst_text;
  UI_Grid cst_grid;
  UI_Button cst_title;
  UI_Button cst_segm;
  UI_Button cst_sfr;
  UI_Button cst_create;
  UI_Button cst_clear;
  
	
	Compiler compiler;
	
	
	public GUI_Controller() {
		root_tabs = new UI_Tabs(0, 0, width, height);
		ui_list.add(root_tabs);
		
		editor = new UI_Editor(0, 0, 0, 0);
		root_tabs.add_tab("Text Editor", editor);
		
		create_tab_tools();
		view_feprom = new UI_Data(mc.FEPROM);
		root_tabs.add_tab("FEPROM-View", view_feprom);
		view_ram = new UI_Data(mc.RAM);
		root_tabs.add_tab("RAM-View", view_ram);
		view_sfr = new UI_Data(mc.RAM_SFR);
		view_sfr.offset = 0x80;
		root_tabs.add_tab("SFR-View", view_sfr);
    create_tab_registers();
    create_tab_custom_registers();
		create_tab_savedfiles();
		
		
		compiler = new Compiler(editor);
	}
	
	
	public void update() {
		// Config
		update_config();
    
    // Registers
    update_custom_registers();
		
		// Execution
		if (btn_run_last_state) {
			float ph = speed_scale.value * 2;
			run_timer += deltatime * ph * ph * ph * ph * 100000;
			while (run_timer > 0.1) {
				run_timer -= 0.1;
				mc.exec();
			
				if (editor.isBreak()) {
					btn_run.setState(false);
					fwq_run.setState(false);
					break;
				}
			}
		}
		view_feprom.marker = mc.PC;
		
		// Update txt info
		if (fw_info_last_state) {
			fwi_txt.txt = "PC: " + hex(mc.PC, 4);
			fwi_txt.txt += "\nDPTR: " + hex(mc.DPTR, 4);
			fwi_txt.txt += "\nSP: " + hex(mc.SP, 2);
			fwi_txt.txt += "\nA: " + hex(mc.A, 2);
			fwi_txt.txt += "\nC: " + mc.C;
			fwi_txt.txt += "\nTCON: " + binary(mc.get_data(0x88), 8);
			fwi_txt.txt += "\nTMOD: " + binary(mc.get_data(0x89), 8);
			fwi_txt.txt += "\nTH0/TL0: " + binary(mc.get_data(0x8C), 8) + " / " + binary(mc.get_data(0x8A), 8);
			fwi_txt.txt += "\nTH1/TL1: " + binary(mc.get_data(0x8D), 8) + " / " + binary(mc.get_data(0x8B), 8);
			
			long ph = mc.TIME;
			int us = (int)(ph % 1000);
			ph = (ph - us) / 1000;
			int ms = (int)(ph % 1000);
			ph = (ph - ms) / 1000;
			fwi_txt.txt += "\nTIME: " + ph + "s " + ms + "ms " + us + "us";
		}
		
		// Save / Load
		if (btn_save.getState()) {
			String title = tf_filename.txt.toUpperCase();
			String allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-";
			boolean is_allowed = true;
			for (int i = 0; i < title.length(); i++) {
				boolean found = false;
				for (int k = 0; k < allowed.length(); k++) {
					if (strcmp(title.charAt(i), allowed.charAt(k))) {
						found = true;
						break;
					}
				}
				
				if (!found) {
					is_allowed = false;
					break;
				}
			}
			
			if (!is_allowed) {
				dprint("Filename may only contain letters, numbers and \"-_\"!", color(255, 0, 0));
			} else if (title.length() == 0) {
				dprint("Enter a new filename!", color(255, 0, 0));
			} else {
				title = tf_filename.txt;
				boolean can_create = true;
				
				String[] saves = loadStrings("savedata.txt");
				if (saves != null) {
					for (int i = 0; i < saves.length; i++) {
						if (saves[i] == title) {
							can_create = false;
							dprint("Filename already taken!", color(255, 0, 0));
							break;
						}
					}
				}
				
				if (can_create) {
					String[] data = new String[editor.get_size()];
					for (int i = 0; i < data.length; i++)
						data[i] = editor.get_line(i);
					
					saveStrings(title, data);
					
					String[] new_saves;
					if (saves != null)
						new_saves = new String[saves.length + 1];
					else
						new_saves = new String[1];
					
					if (saves != null) {
						for (int i = 0; i < saves.length; i++) {
							new_saves[i] = saves[i];
						}
					}
					new_saves[new_saves.length - 1] = title;
					
					saveStrings("savedata.txt", new_saves);
					
					dprint("File \"" + title + "\" saved!", color(0, 255, 0));
					tf_filename.txt = "";
					file_list.add_item(title);
				}
			}
		}
		
		if (btn_load.getState()) {
			String sel = file_list.get_selected();
			
			if (sel != "") {
				String[] data = loadStrings(sel);
				if (data != null) {
					if (data == null || data.length == 0) {
						data = new String[1];
						data[0] = "?";
					}
					editor.load_data(data);
					dprint("File \"" + sel + "\" loaded!", color(0, 255, 0));
				} else {
					dprint("File not found!", color(255, 0, 0));
				}
			} else {
				dprint("Nothing is selected!", color(255, 0, 0));
			}
		}
		
		if (btn_overwrite.getState()) {
			String sel = file_list.get_selected();
			
			if (sel != null && !strcmp(sel, "")) {
        // ..store old data as well
        String[] prev_data = loadStrings(sel);
        saveStrings(sel + ".old", prev_data);
        
        // ..overwrite
				String[] data = new String[editor.get_size()];
				for (int i = 0; i < data.length; i++)
					data[i] = editor.get_line(i);
				
				saveStrings(sel, data);
				dprint("File \"" + sel + "\" overwritten!", color(255, 0, 0));
			} else {
				dprint("Nothing is selected!", color(255, 0, 0));
			}
		}
		
		if (btn_delete.getState()) {
			String sel = file_list.get_selected();
			
			if (sel.length() != 0) {
				file_list.remove_item(sel);
				dprint("Deleted \"" + sel + "\"!", color(255, 0, 0));
				
				String[] saves = loadStrings("savedata.txt");
				if (saves != null) {
					boolean exists = false;
					for (int i = 0; i < saves.length; i++) {
						if (strcmp(saves[i], sel)) {
							exists = true;
							break;
						}
					}
					
					if (exists) {
						if (saves.length == 1) {
							String[] ph = new String[1];
							ph[0] = "";
							saveStrings("savedata.txt", ph);
						} else {
							String[] ph = new String[saves.length - 1];
							int k = 0;
							for (int i = 0; i < saves.length; i++) {
								if (strcmp(saves[i], sel))
									continue;
								
								ph[k] = saves[i];
								k++;
							}
							
							saveStrings("savedata.txt", ph);
						}
					}
				}
			} else {
				dprint("Nothing is selected!", color(255, 0, 0));
			}
		}
	}
	
	public void update_config() {
    if (btn_markpc.getState() != editor.mark_line)
      editor.mark_line = btn_markpc.getState();
    
		if (btn_dprint.getState())
			draw_dprint();
		
		if (btn_compile.getState() || fwq_compile.getState())
			compiler.compile();
		
		if (btn_reset.getState() || fwq_reset.getState()) {
			mc.light_reset();
			dprint("Controller reset", color(0, 255, 0));
		}
		
		// Run buttons
		if (btn_run.getState() != btn_run_last_state) {
			btn_run_last_state = btn_run.getState();
			fwq_run_last_state = btn_run_last_state;
			fwq_run.setState(btn_run_last_state);
		} else if (fwq_run.getState() != fwq_run_last_state) {
			fwq_run_last_state = fwq_run.getState();
			btn_run_last_state = fwq_run_last_state;
			btn_run.setState(fwq_run_last_state);
		}
    
    // Etc
    if (btn_clear.getState()) {
      String[] data = { "" };
      editor.load_data(data);
      dprint("Text cleared", color(255, 0, 0));
    }
    
    if (btn_clear_debug.getState()) {
      for (int i = (debug_output.size() - 1); i >= 0; i--) {
        debug_output.remove(i);
        debug_output_color.remove(i);
      }
    }
		
		// Floating windows
		if (fw_quick_last_state != btn_quick_access.getState()) {
			fw_quick_last_state = btn_quick_access.getState();
			if (fw_quick_last_state) {
				// Show window
				ui_prepend(fw_quick);
			} else {
				// Hide window
				for (int i = 0; i < ui_list.size(); i++) {
					if (ui_list.get(i) == fw_quick) {
						ui_list.remove(i);
						break;
					}
				}
			}
		}
		if (fw_info_last_state != btn_info.getState()) {
			fw_info_last_state = btn_info.getState();
			if (fw_info_last_state) {
				// Show window
				ui_prepend(fw_info);
			} else {
				// Hide window
				for (int i = 0; i < ui_list.size(); i++) {
					if (ui_list.get(i) == fw_info) {
						ui_list.remove(i);
						break;
					}
				}
			}
		}
		
		if (fwq_step.getState()) {
			mc.exec();
		}
	}
	
	
	// TABS //
	private void create_tab_tools() {
		UI_Grid grid = new UI_Grid(6, 14, 0, 0, 0, 0);
		root_tabs.add_tab("Tools&Sim", grid);
		
		btn_dprint = new UI_Button("Debug-Print", true, 0, 0, 0, 0);
		grid.add_child(btn_dprint, 0, 0, 2, 1);
		btn_dprint.setState(true);
		
		speed_scale = new UI_Scale("Speed", 0.5, 0, 0, 0, 0);
		grid.add_child(speed_scale, 2, 0, 2, 1);
		
		btn_markpc = new UI_Button("Mark PC", true, 0, 0, 0, 0);
		grid.add_child(btn_markpc, 4, 0, 2, 1);
		btn_markpc.setState(true);
		
		btn_compile = new UI_Button("Compile", false, 0, 0, 0, 0);
		grid.add_child(btn_compile, 0, 2, 2, 1);
		
		btn_reset = new UI_Button("Reset", false, 0, 0, 0, 0);
		grid.add_child(btn_reset, 2, 2, 2, 1);
		
		btn_run = new UI_Button("Run", true, 0, 0, 0, 0);
		grid.add_child(btn_run, 4, 2, 2, 1);
		btn_run_last_state = btn_run.getState();
    
    btn_clear = new UI_Button("Clear Text", false, 0, 0, 0, 0);
    grid.add_child(btn_clear, 0, 3, 2, 1);
    
    btn_clear_debug = new UI_Button("Clear Debug", false, 0, 0, 0, 0);
    grid.add_child(btn_clear_debug, 2, 3, 2, 1);
		
		btn_quick_access = new UI_Button("Quick-Access Window", true, 0, 0, 0, 0);
		grid.add_child(btn_quick_access, 1, 5, 4, 1);
		
		btn_info = new UI_Button("Controller-Info Window", true, 0, 0, 0, 0);
		grid.add_child(btn_info, 1, 6, 4, 1);
		
		// Floating windows
		float phW = width / 4;
		float phH = height / 4;
		fw_quick = new UI_FloatingWindow((width - phW) / 2, (height - phH) / 2, phW, phH);
		fw_quick_last_state = btn_quick_access.getState();
		UI_Grid fwq_grid = new UI_Grid(2, 2, 0, 0, 0, 0);
		fw_quick.set_child(fwq_grid);
		
		fwq_step = new UI_Button("Step", false, 0, 0, 0, 0);
		fwq_grid.add_child(fwq_step, 0, 0, 1, 1);
		fwq_run = new UI_Button("Run", true, 0, 0, 0, 0);
		fwq_grid.add_child(fwq_run, 1, 0, 1, 1);
		fwq_reset = new UI_Button("Reset", false, 0, 0, 0, 0);
		fwq_grid.add_child(fwq_reset, 0, 1, 1, 1);
		fwq_compile = new UI_Button("Compile", false, 0, 0, 0, 0);
		fwq_grid.add_child(fwq_compile, 1, 1, 1, 1);
		fwq_run_last_state = fwq_run.getState();
		
		
		phW *= 1.2f;
		phH *= 1.3f;
		fw_info = new UI_FloatingWindow((width - phW) / 2, (height - phH) / 2, phW, phH);
		fw_info_last_state = btn_quick_access.getState();
		UI_Grid fwi_grid = new UI_Grid(2, 2, 0, 0, 0, 0);
		fw_info.set_child(fwi_grid);
		
		fwi_txt = new UI_Text("", 0, 0, 0, 0);
		fwi_grid.add_child(fwi_txt, 0, 0, 2, 2);
		fwi_txt.h = 28;
	}
	
	private void create_tab_savedfiles() {
		UI_Grid grid = new UI_Grid(8, 18, 0, 0, 0, 0);
		root_tabs.add_tab("Saved Files", grid);
		
		// New File
		
		UI_Text txt1 = new UI_Text("Create new file\n* Whitespaces not allowed!", 0, 0, 0, 0);
		grid.add_child(txt1, 0, 0, 1, 1);
		
		tf_filename = new UI_TextField("File Name", 0, 0, 0, 0);
		grid.add_child(tf_filename, 2, 2, 4, 1);
		
		btn_save = new UI_Button("Save", false, 0, 0, 0, 0);
		grid.add_child(btn_save, 5, 3, 1, 1);
		
		// Load File
		
		UI_Text txt2 = new UI_Text("Load file\n* You might lose unsaved data!", 0, 0, 0, 0);
		grid.add_child(txt2, 0, 6, 1, 1);
		
		file_list = new UI_List(0, 0, 0, 0);
		grid.add_child(file_list, 2, 8, 4, 8);
		
		btn_load = new UI_Button("Load", false, 0, 0, 0, 0);
		grid.add_child(btn_load, 6, 8, 1, 1);
		btn_load.default_color = color(100, 255, 100);
		
		btn_overwrite = new UI_Button("Overwrite", false, 0, 0, 0, 0);
		grid.add_child(btn_overwrite, 6, 10, 1, 1);
		btn_overwrite.default_color = color(255, 255, 100);
		
		btn_delete = new UI_Button("Delete", false, 0, 0, 0, 0);
		grid.add_child(btn_delete, 6, 15, 1, 1);
		btn_delete.default_color = color(255, 100, 100);
		
		String[] saves = loadStrings("savedata.txt");
		if (saves != null) {
			for (int i = 0; i < saves.length; i++) {
        if (saves[i].length() > 0)
				  file_list.add_item(saves[i]);
			}
		}
	}
  
  private void create_tab_registers() {
    UI_Grid grid = new UI_Grid(7, 26, 0, 0, 0, 0);
    root_tabs.add_tab("Registers", grid);
    
    // Ports
    UI_Register reg = new UI_Register("Port 0", 0x80, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 0, 1, 3, 2);
    
    reg = new UI_Register("Port 1", 0x90, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 0, 4, 3, 2);
    
    reg = new UI_Register("Port 2", 0xA0, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 0, 7, 3, 2);
    
    reg = new UI_Register("Port 3", 0xB0, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 0, 10, 3, 2);
    
    reg = new UI_Register("Port 4", 0xC0, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 0, 13, 3, 2);
    
    reg = new UI_Register("Port 5", 0xE8, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 0, 16, 3, 2);
    
    // Default registers
    reg = new UI_Register("R0", 0, false, 0, 0, 0, 0);
    grid.add_child(reg, 0, 19, 3, 2);
    
    reg = new UI_Register("R1", 1, false, 0, 0, 0, 0);
    grid.add_child(reg, 0, 22, 3, 2);
    
    reg = new UI_Register("R2", 2, false, 0, 0, 0, 0);
    grid.add_child(reg, 4, 1, 3, 2);
    
    reg = new UI_Register("R3", 3, false, 0, 0, 0, 0);
    grid.add_child(reg, 4, 4, 3, 2);
    
    reg = new UI_Register("R4", 4, false, 0, 0, 0, 0);
    grid.add_child(reg, 4, 7, 3, 2);
    
    reg = new UI_Register("R5", 5, false, 0, 0, 0, 0);
    grid.add_child(reg, 4, 10, 3, 2);
    
    reg = new UI_Register("R6", 6, false, 0, 0, 0, 0);
    grid.add_child(reg, 4, 13, 3, 2);
    
    reg = new UI_Register("R7", 7, false, 0, 0, 0, 0);
    grid.add_child(reg, 4, 16, 3, 2);
    
    // Other
    reg = new UI_Register("TCON", 0x88, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 4, 19, 3, 2);
    reg = new UI_Register("TMOD", 0x89, false, 0, 0, 0, 0);
    reg.set_sfr(true);
    grid.add_child(reg, 4, 22, 3, 2);
  }
  
  
  private void update_custom_registers() {
    if (cst_clear.getState()) {
      for (int i = (cst_cont.child.size() - 1); i >= 0; i--) {
        UI_Widget w = cst_cont.child.get(i);
        
        if (w != cst_grid)
          cst_cont.child.remove(i);
      }
    }
    
    if (cst_create.getState()) {
      int adr = compiler.get_type(cst_text.txt);
      
      if (adr != CMP_DADR) {
        dprint("Address invalid!", color(255, 0, 0));
      } else {
        adr = compiler.read_address(cst_text.txt);
        
        boolean seg7 = cst_segm.getState();
        boolean sfr = cst_sfr.getState();
        
        if (sfr) {
          if (adr < 0x80 || adr > 0xFF) {
            dprint("Address invalid!", color(255, 0, 0));
            return;
          }
        }
        
        UI_Register reg;
        if (seg7) {
          reg = new UI_Register((cst_title.getState() ? cst_text.txt : ""), adr, true, 50, 100, 150, 200);
        } else {
          reg = new UI_Register((cst_title.getState() ? cst_text.txt : ""), adr, false, 50, 100, 50 * 8, 50);
        }
        
        if (sfr) {
          reg.set_sfr(true);
        }
        
        cst_cont.add_child(reg);
        reg.set_movable(true);
      }
    }
  }
  
  private void create_tab_custom_registers() {
    cst_cont = new UI_Container(0, 0, 0, 0);
    root_tabs.add_tab("Custom Registers", cst_cont);
    
    cst_grid = new UI_Grid(10, 1, 0, 0, cst_cont.w, 40);
    cst_cont.add_child(cst_grid);
    
    cst_text = new UI_TextField("Address", 0, 0, 0, 0);
    cst_grid.add_child(cst_text, 0, 0, 2, 1);
    
    cst_title = new UI_Button("Show title", true, 0, 0, 0, 0);
    cst_title.setState(true);
    cst_grid.add_child(cst_title, 2, 0, 2, 1);
    
    cst_segm = new UI_Button("7-Segment", true, 0, 0, 0, 0);
    cst_grid.add_child(cst_segm, 4, 0, 2, 1);
    
    cst_sfr = new UI_Button("SFR", true, 0, 0, 0, 0);
    cst_grid.add_child(cst_sfr, 6, 0, 1, 1);
    
    cst_create = new UI_Button("Create", false, 0, 0, 0, 0);
    cst_grid.add_child(cst_create, 7, 0, 1, 1);
    
    cst_clear = new UI_Button("x", false, 0, 0, 0, 0);
    cst_grid.add_child(cst_clear, 9, 0, 1, 1);
  }
}
