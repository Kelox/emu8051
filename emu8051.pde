
// GUI //
public ArrayList<UI_Widget> ui_list = new ArrayList<UI_Widget>();
public UI_Widget ui_active = null;

public GUI_Controller gui;
public Microcontroller mc = new Microcontroller();


// TEXT WRITING //
int[] txt_disallowed = {
	8, 10, 16, 17, 18, 157, 65535, 222, 219, 37, 38, 39, 40, 65406
}; // 8-Backspace; 10-Enter; 37-left; 38-up; 39-right; 40-down
final int TXT_BACKSPACE = 8;
final int TXT_ENTER = 10;
final int TXT_LEFT = 37;
final int TXT_UP = 38;
final int TXT_RIGHT = 39;
final int TXT_DOWN = 40;

final String[] txt_keywords = {
	"MOV", "POP", "PUSH", "NOP", "XCH", "XCHD", "MOVX", "MOVC", "CLR", "CPL", "INC",
	"DEC", "ADD", "ADDC", "DA", "SUBB", "SWAP", "MUL", "DIV", "RL", "RLC", "RR", "RRC",
	"SETB", "CLR", "CPL", "ANL", "ORL", "XRL", "LJMP", "SJMP", "AJMP", "JMP", "JBC",
	"JB", "JNB", "JC", "JNC", "JZ", "JNZ", "DJNZ", "CJNE", "LCALL", "ACALL", "CALL",
	"RET", "RETI", "ORG", "DB"
};


// COMPILER //
final int CMP_UNKNOWN = -1;
final int CMP_A = 0;
final int CMP_DADR = 1;
final int CMP_BADR = 2;
final int CMP_CONST = 3;
final int CMP_DPTR = 4;
final int CMP_SFR = 5; // address inside sfr <--- UNUSED!!
final int CMP_SFRB = 6; // bit-address inside sfr <--- UNUSED!!
final int CMP_REGISTER = 7;
final int CMP_POINTER = 8; // @Rn
final int CMP_CARRY = 9;
final int CMP_A_DPTR = 10;
final int CMP_A_PC = 11;
final int CMP_INV_BADR = 12;

final String[] cmp_shortcut = {
	"P0", "80h",
	"P1", "90h",
	"P2", "A0h",
	"P3", "B0h",
	"P4", "C0h",
	"P5", "E8h",
	"TCON", "88h",
	"TMOD", "89h",
	"TL0", "8Ah",
	"TL1", "8Bh",
	"TH0", "8Ch",
	"TH1", "8Dh",
	"IE", "A8h"
};

final String[] cmp_bit_shortcut = {
	"TR0", "88h.4",
	"TF0", "88h.5",
	"TR1", "88h.6",
	"TF1", "88h.7",
	"IT0", "88h.0",
	"IE0", "88h.1",
	"IT1", "88h.2",
	"IE1", "88h.3",
	"EX0", "A8h.0",
	"ET0", "A8h.1",
	"EX1", "A8h.2",
	"ET1", "A8h.3",
	"ES", "A8h.4",
	"ET2", "A8h.5",
	"ECA", "A8h.6",
	"EA", "A8h.7",
  "T0", "B0h.4",
  "T1", "B0h.5"
};


// ELSE //
long lastMillis = 0;
float deltatime = 0;


// DEBUG //
private ArrayList<String> debug_output = new ArrayList<String>();
private ArrayList<Integer> debug_output_color = new ArrayList<Integer>();


void setup() {
  size(1000, 700);
	
	dprint("-- Willkommen --", color(255));
	dprint("-> Debug-Anzeige laesst sich ueber die Einstellungen deaktivieren", color(255));
	dprint("-> Code muss nach jeder Veraenderung erneut kompiliert werden", color(255));
  dprint("-> \"include\" ist nicht noetig", color(255));
  dprint("-> Jede Zeile darf max. 1x Befehl beinhalten", color(255));
  dprint("-> Interrupt Prioritaeten nicht implementiert", color(255));
	dprint("", color(255));
	
	gui = new GUI_Controller();
}

void draw() {
	// Deltatime
	deltatime = (millis() - lastMillis) / 1000f;
	lastMillis = millis();
	
  background(255);
	fill(0);
	
	
	// Draw UI
	for (int i = (ui_list.size() - 1); i >= 0; i--) {
		ui_list.get(i).draw();
	}
	
	// Input
	if (mousePressed != ph_mouse_was_down) {
		if (mousePressed) {
			mouse_pressed();
		} else {
			mouse_released();
			ph_mouse_x = -1;
			ph_mouse_y = -1;
		}
		
		ph_mouse_was_down = mousePressed;
	}
	
	if (mousePressed) {
		boolean can_drag = (ph_mouse_x == -1 ? false : true);
		int dx = mouseX - ph_mouse_x;
		int dy = mouseY - ph_mouse_y;
		ph_mouse_x = mouseX;
		ph_mouse_y = mouseY;
		
		if (dx != 0 || dy != 0)
			if (can_drag)
				mouse_dragged(dx, dy);
	}
	
	// Update gui
	gui.update();
}


// INPUT //
private boolean ph_mouse_was_down = false;
private int ph_mouse_down_x = -1; // first down position
private int ph_mouse_down_y = -1;
private int ph_mouse_x = -1; // updated down position
private int ph_mouse_y = -1;

// the methods given by processing are brain dead
private void mouse_pressed() {
	ph_mouse_down_x = mouseX;
	ph_mouse_down_y = mouseY;
	
	for (int i = 0; i < ui_list.size(); i++) {
		if (ui_list.get(i).contains(mouseX, mouseY)) {
			ui_active = ui_list.get(i);
			break;
		}
	}
}
private void mouse_released() {
	// Widgets
	for (int i = 0; i < ui_list.size(); i++) {
		if (ui_list.get(i).contains(mouseX, mouseY)) {
			UI_Widget w = ui_list.get(i);
			w.released(mouseX - w.x, mouseY - w.y);
			break;
		}
	}
	
	// Click
	float dis = dist(ph_mouse_down_x, ph_mouse_down_y, mouseX, mouseY);
	if (dis < 4)
		mouse_clicked();
}
private void mouse_clicked() {
	ui_active = null;
	
	// Widgets
	for (int i = 0; i < ui_list.size(); i++) {
		if (ui_list.get(i).contains(mouseX, mouseY)) {
			UI_Widget w = ui_list.get(i);
			w.clicked(mouseX, mouseY);
			break;
		}
	}
}
private void mouse_dragged(int dx, int dy) {
	if (ui_active == null)
		return;
	
	ui_active.dragged(ph_mouse_x, ph_mouse_y, dx, dy);
}


public void keyPressed() {
	if (ui_active == null)
		return;
	
	//dprint("ok" + (int)key + " " + key + " " + keyCode);
	ui_active.keypressed((int)key, keyCode);
	//dprint("Key " + str(key));
}


// DEBUG //
public void dprint(String str, color clr) {
	String h = "" + hour();
	String m = "" + minute();
	if (h.length() == 1)
		h = "0" + h;
	if (m.length() == 1)
		m = "0" + m;
	
	debug_output.add("[" + h + ":" + m + "]\t" + str);
	debug_output_color.add(clr);
	
	if (debug_output.size() == 21) {
		debug_output.remove(0);
		debug_output_color.remove(0);
	}
}
public void draw_dprint() {
	textSize(18);
	textAlign(LEFT, BOTTOM);
	
	for (int i = 0; i < debug_output.size(); i++) {
		float ph = (debug_output.size() - i - 1) * 18;
		
		// Draw shadow
		fill(0);
		for (int x = -1; x < 2; x++)
			for (int y = -1; y < 2; y++)
				text(debug_output.get(i), 10 + x, height - ph - 10 + y);
		
		// Draw color
		fill(debug_output_color.get(i));
		text(debug_output.get(i), 10, height - ph - 10);
	}
}


// USEFUL //
public void stext(String s, float px, float py, color clr) {
	fill(0);
	for (int x = -1; x < 2; x++)
			for (int y = -1; y < 2; y++)
				text(s, px + x, py + y);
	
	fill(clr);
	text(s, px, py);
}


public void ui_prepend(UI_Widget w) {
	ArrayList<UI_Widget> ph = new ArrayList<UI_Widget>();
	ph.add(w);
	for (int i = 0; i < ui_list.size(); i++) {
		ph.add(ui_list.get(i));
	}
	ui_list = ph;
}


public boolean strcmp(String a, String b)
{
  return a.equals(b);
}
public boolean strcmp(String a, char b)
{
  return strcmp(a, str(b));
}
public boolean strcmp(char a, String b)
{
  return strcmp(str(a), b);
}
public boolean strcmp(char a, char b)
{
  return strcmp(str(a), str(b));
}
