public class UI_DragBox extends UI_Widget {
	private ArrayList<UI_Widget> child;
	private boolean hDrag, vDrag;
	private int dragX, dragY;
	private int max_drag_x, max_drag_y;
	
	
	public UI_DragBox(boolean phDrag, boolean pvDrag, int px, int py, int pw, int ph) {
		super(px, py, pw, ph);
		
		child = new ArrayList<UI_Widget>();
		
		hDrag = phDrag;
		vDrag = pvDrag;
		
		dragX = 0;
		dragY = 0;
		max_drag_x = 0;
		max_drag_y = 0;
	}
	
	
	public void draw() {
		// Draw Background
		strokeWeight(3);
		stroke(0);
		fill(255);
		
		rect(x, y, w, h, 16);
		
		// Draw children
		translate(x + dragX, y + dragY);
		//clip(0, 0, w, h); // doesnt work on ios
		
		
		for (int i = (child.size() - 1); i >= 0; i--) {
			UI_Widget widget = child.get(i);
			
			float cenX = widget.x + widget.w * 0.5 + dragX;
			float cenY = widget.y + widget.h * 0.5 + dragY;
			
			
			widget.draw();
		}
		
		translate(-x - dragX, -y - dragY);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		for (int i = 0; i < child.size(); i++) {
			UI_Widget w = child.get(i);
			if (w.contains((int)(px - x - dragX), (int)(py - y - dragY))) {
				w.clicked(px - x - dragY, py - y - dragY);
			}
		}
	}
	public void dragged(float px, float py, float dx, float dy) {
		dragX += dx;
		dragY += dy;
		
		dragX = (dragX < 0 ? 0 : dragX);
		dragX = (dragX > max_drag_x ? max_drag_x : dragX);
		dragY = (dragY < 0 ? 0 : dragY);
		dragY = (dragY > max_drag_y ? max_drag_y : dragY);
	}
	public void keypressed(int k, int kcode) {
		
	}
	
	public void add_child(UI_Widget c) {
		child.add(c);
		max_drag_x = (int)(max_drag_x < c.x ? c.x : max_drag_x);
		max_drag_y = (int)(max_drag_y < c.y ? c.y : max_drag_y);
	}
}
