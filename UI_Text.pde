public class UI_Text extends UI_Widget {
	private String txt;
	public int halign;
	public int valign;
	
	public UI_Text(String pTxt, int px, int py, int pw, int ph) {
		super(px, py, pw, ph);
		
		txt = pTxt;
		halign = LEFT;
		valign = TOP;
	}
	
	public void draw() {
		textAlign(halign, valign);
		textSize(h / 1.5);
    textLeading(h / 1.5);
		stext(txt, x, y, color(255));
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		
	}
	public void dragged(float px, float py, float dx, float dy) {
		
	}
	public void keypressed(int k, int kcode) {
		
	}
}
