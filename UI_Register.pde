public class UI_Register extends UI_Widget {
  private int address;
  private String title;
  
  private boolean movable = false;
  private boolean seg7 = false;
  private boolean sfr = false;
  
  private final float segWidth = 20;
  
  
  public UI_Register(String pTitle, int pAddress, boolean pSeg7, float px, float py, float pw, float ph) {
    super(px, py, pw, ph);
    
    address = pAddress;
    title = pTitle;
    seg7 = pSeg7;
  }
  
  public void draw() {
    stroke(0, 0, 0, 255);
    
    float ph_x = x;
    float ph_y = y;
    
    if (movable) {
      x = (round(x / 25) * 25);
      y = (round(y / 25) * 25);
    }
    
    int v = mc.RAM[address];
    if (sfr)
      v = mc.get_data(address);
    
    if (seg7) {
      fill(55, 55, 55);
      rect(x, y, w, h);
      
      for (int i = 0; i < 8; i++) {
        if ((v & (1 << i)) != 0)
          fill(255, 55, 55);
        else
          fill(75, 75, 75);
        
        switch (i) {
          case (0):
            rect(x + 10 + segWidth, y + 10, w - 20 - segWidth * 2, segWidth);
            break;
          case (1):
            rect(x + w - 10 - segWidth, y + 10 + segWidth, segWidth, (h - 20 - segWidth * 3) / 2);
            break;
          case (2):
            rect(x + w - 10 - segWidth, y + 10 + segWidth * 2 + ((h - 20 - segWidth * 3) / 2), segWidth, (h - 20 - segWidth * 3) / 2);
            break;
          case (3):
            rect(x + 10 + segWidth, y + h - 10 - segWidth, w - 20 - segWidth * 2, segWidth);
            break;
          case (4):
            rect(x + 10, y + 10 + segWidth * 2 + ((h - 20 - segWidth * 3) / 2), segWidth, (h - 20 - segWidth * 3) / 2);
            break;
          case (5):
            rect(x + 10, y + 10 + segWidth, segWidth, (h - 20 - segWidth * 3) / 2);
            break;
          case (6):
            rect(x + 10 + segWidth, y + 10 + ((h - 20 - segWidth * 3) / 2) + segWidth, w - 20 - segWidth * 2, segWidth);
            break;
        }
      }
    } else {
      for (int i = 0; i < 8; i++) {
        if ((v & (1 << i)) != 0)
          fill(255, 55, 55);
        else
          fill(55, 55, 55);
        
        rect(x + (w / 8) * (7 - i), y, w / 8, h);
      }
    }
    
    textAlign(LEFT, CENTER);
    textSize(24);
    stext(title, x, y - 5, color(255));
    
    if (movable) {
      x = ph_x;
      y = ph_y;
    }
  }
  public void pressed(float px, float py) {
    
  }
  public void released(float px, float py) {
    if (movable) {
      x = (round(x / 25) * 25);
      y = (round(y / 25) * 25);
    }
  }
  public void clicked(float px, float py) {
    if (seg7 || address < 0 || address > 0xFF)
      return;
    
    px -= x;
    py -= y;
    
    int idx = 7 - (int)floor(px / (w / 8));
    
    if (idx >= 0 && idx < 8) {
      if (sfr) {
        int v = mc.get_data(address);
        mc.set_data(address, v ^ (1 << idx));
      } else {
        int v = mc.RAM[address];
        mc.RAM[address] = v ^ (1 << idx);
      }
    }
  }
  public void dragged(float px, float py, float dx, float dy) {
    if (movable) {
      x += dx;
      y += dy;
      
      ui_active = this;
    }
  }
  public void keypressed(int k, int kcode) {
    
  }
  
  public void set_movable(boolean p) {
    movable = p;
  }
  
  public void set_sfr(boolean p) {
    sfr = p;
  }
}
