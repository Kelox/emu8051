public class Microcontroller {
	// Direkt addressierbar: RAM 0->7F SFR 80->FF
	// Indirekt addressierbar: RAM 80->FF
	public int[] RAM = new int[256];
	public int[] RAM_SFR = new int[128];
	public int[] FEPROM = new int[64000];
	
	public int PC; // Program Counter
	public int A; // Accumulator
	public int SP; // Stack Pointer
	public int DPTR;
	public int C;
	
	public long TIME; // in micro seconds
  
  public int prevP3;
  public int prevTCON;
	
	
	public Microcontroller() {
		reset();
	}
	
	
	public void reset() {
		light_reset();
		
		for (int i = 0; i < FEPROM.length; i++)
			FEPROM[i] = 0xFF;
	}
	public void light_reset() {
		PC = 0;
		A = 0;
		SP = 7;
		DPTR = 0;
		TIME = 0;
		C = 0;
		
		for (int i = 0; i < RAM.length; i++)
			RAM[i] = 0;
		
		for (int i = 0; i < RAM_SFR.length; i++)
			RAM_SFR[i] = 0;
    
    prevP3 = 0;
    prevTCON = 0;
	}
	
	public void exec() {
		long deltaTime = TIME;
    
		
		int ph = 0;
		// Execute next command
		switch (FEPROM[PC]) {
			// DATENTRANSPORT //
			case (0x74): // MOV A, #c8
				A = FEPROM[PC+1];
				PC += 2;
				TIME += 1;
				break;
			case (0x78): // MOV Rn, #c8
			case (0x79):
			case (0x7A):
			case (0x7B):
			case (0x7C):
			case (0x7D):
			case (0x7E):
			case (0x7F):
				RAM[FEPROM[PC]-0x78] = FEPROM[PC+1];
				PC += 2;
				TIME += 1;
				break;
			case (0x75): // MOV dadr, #c8
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = FEPROM[PC+2];
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = FEPROM[PC+2];
				PC += 3;
				TIME += 2;
				break;
			case (0xE8): // MOV A, Rn
			case (0xE9):
			case (0xEA):
			case (0xEB):
			case (0xEC):
			case (0xED):
			case (0xEE):
			case (0xEF):
				A = RAM[FEPROM[PC]-0xE8];
				PC += 1;
				TIME += 1;
				break;
			case (0xF8): // MOV Rn, A
			case (0xF9):
			case (0xFA):
			case (0xFB):
			case (0xFC):
			case (0xFD):
			case (0xFE):
			case (0xFF):
				RAM[FEPROM[PC]-0xF8] = A;
				PC += 1;
				TIME += 1;
				break;
			case (0xE5): // MOV A, dadr
				if (FEPROM[PC+1] < 0x80)
					A = RAM[FEPROM[PC+1]];
				else
					A = RAM_SFR[FEPROM[PC+1]-0x80];
				PC += 2;
				TIME += 1;
				break;
			case (0xF5): // MOV dadr, A
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = A;
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = A;
				PC += 2;
				TIME += 1;
				break;
			case (0xA8): // MOV Rn, dadr
			case (0xA9):
			case (0xAA):
			case (0xAB):
			case (0xAC):
			case (0xAD):
			case (0xAE):
			case (0xAF):
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC]-0xA8] = RAM[FEPROM[PC+1]];
				else
					RAM[FEPROM[PC]-0xA8] = RAM_SFR[FEPROM[PC+1]-0x80];
				PC += 2;
				TIME += 2;
				break;
			case (0x88): // MOV dadr, Rn
			case (0x89):
			case (0x8A):
			case (0x8B):
			case (0x8C):
			case (0x8D):
			case (0x8E):
			case (0x8F):
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC]-0x88];
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM[FEPROM[PC]-0x88];
				PC += 2;
				TIME += 2;
				break;
			case (0x85): // MOV dadr, dadr
				if (FEPROM[PC+1] < 0x80) {
					if (FEPROM[PC+2] < 0x80)
						RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+2]];
					else
						RAM[FEPROM[PC+1]] = RAM_SFR[FEPROM[PC+2]-0x80];
				} else {
					if (FEPROM[PC+2] < 0x80)
						RAM_SFR[FEPROM[PC+1]-0x80] = RAM[FEPROM[PC+2]];
					else
						RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+2]-0x80];
				}
				PC += 3;
				TIME += 2;
				break;
			case (0xE6): // MOV A, @Rn
			case (0xE7):
				A = RAM[RAM[FEPROM[PC]-0xE6]];
				PC += 1;
				TIME += 1;
				break;
			case (0xF6): // MOV @Rn, A
			case (0xF7):
				RAM[RAM[FEPROM[PC]-0xF6]] = A;
				PC += 1;
				TIME += 1;
				break;
			case (0x86): // MOV dadr, @Rn
			case (0x87):
				RAM[FEPROM[PC+1]] = RAM[RAM[FEPROM[PC]-0x86]];
				PC += 2;
				TIME += 2;
				break;
			case (0x76): // MOV @Rn, #c8
			case (0x77):
				RAM[RAM[FEPROM[PC]-0x76]] = FEPROM[PC+1];
				PC += 2;
				TIME += 1;
				break;
			case (0xA6): // MOV @Rn, dadr
			case (0xA7):
				RAM[RAM[FEPROM[PC]-0xA6]] = get_data(FEPROM[PC+1]);
				PC += 2;
				TIME += 2;
				break;
			case (0xD0): // POP dadr
				RAM[FEPROM[PC+1]] = RAM[SP];
				SP = (SP + 255) % 256;
				PC += 2;
				TIME += 2;
				break;
			case (0xC0): // PUSH dadr
				SP = (SP + 1) % 256;
				RAM[SP] = RAM[FEPROM[PC+1]];
				PC += 2;
				TIME += 2;
				break;
			case (0x00): // NOP
				PC += 1;
				TIME += 1;
				break;
			case (0x92): // MOV badr, C
				if (C != 0)
					set_badr(FEPROM[PC+1]);
				else
					clr_badr(FEPROM[PC+1]);
				PC += 2;
				TIME += 2;
				break;
			case (0xA2): // MOV C, badr
				if (get_badr(FEPROM[PC+1]) != 0)
					C = 1;
				else
					C = 0;
				PC += 2;
				TIME += 1;
				break;
			case (0x90): // MOV DPTR, #c16
				DPTR = FEPROM[PC+1];
				DPTR = (DPTR << 8) | FEPROM[PC+2];
				PC += 3;
				TIME += 2;
				break;
			case (0xC8): // XCH A, Rn
			case (0xC9):
			case (0xCA):
			case (0xCB):
			case (0xCC):
			case (0xCD):
			case (0xCE):
			case (0xCF):
				ph = A;
				A = RAM[FEPROM[PC]-0xC8];
				RAM[FEPROM[PC]-0xC8] = ph;
				PC += 1;
				TIME += 1;
				break;
			case (0xC5): // XCH A, dadr
				ph = A;
				A = get_data(FEPROM[PC+1]);
				set_data(FEPROM[PC+1], ph);
				PC += 2;
				TIME += 1;
				break;
			case (0xC6): // XCH A, @Rn
			case (0xC7):
				ph = A;
				A = RAM[RAM[FEPROM[PC]-0xC6]];
				RAM[RAM[FEPROM[PC]-0xC6]] = ph;
				PC += 1;
				TIME += 1;
				break;
			case (0x93): // MOVC A, @A+DPTR
				A = FEPROM[(A+DPTR)%65536];
				PC += 1;
				TIME += 2;
				break;
			case (0x83): // MOVC A, @A+PC
				A = FEPROM[(A+PC)%65536];
				PC += 1;
				TIME += 2;
				break;
			// ARITHMETISCHE OPERATOREN //
			case (0xE4): // CLR A
				A = 0;
				PC += 1;
				TIME += 1;
				break;
			case (0xF4): // CPL A
				A = A ^ 0b11111111;
				PC += 1;
				TIME += 1;
				break;
			case (0x04): // INC A
				A = (A + 1) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x08): // INC Rn
			case (0x09):
			case (0x0A):
			case (0x0B):
			case (0x0C):
			case (0x0D):
			case (0x0E):
			case (0x0F):
				RAM[FEPROM[PC] - 0x08] = (RAM[FEPROM[PC] - 0x08] + 1) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x05): // INC dadr
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = (RAM[FEPROM[PC+1]] + 1) % 256;
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = (RAM_SFR[FEPROM[PC+1]-0x80] + 1) % 256;
				PC += 2;
				TIME += 1;
				break;
			case (0xA3): // INC DPTR
				DPTR = (DPTR + 1) % 65536;
				PC += 1;
				TIME += 2;
				break;
			case (0x06): // INC @R0/@R1
			case (0x07):
				RAM[RAM[FEPROM[PC]-0x06]] = (RAM[RAM[FEPROM[PC]-0x06]] + 1) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x14): // DEC A
				A = (A + 255) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x18): // DEC Rn
			case (0x19):
			case (0x1A):
			case (0x1B):
			case (0x1C):
			case (0x1D):
			case (0x1E):
			case (0x1F):
				RAM[FEPROM[PC]-0x18] = (RAM[FEPROM[PC]-0x18] + 255) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x15): // DEC dadr
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = (RAM[FEPROM[PC+1]] + 255) % 256;
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = (RAM_SFR[FEPROM[PC+1]-0x80] + 255) % 256;
				PC += 2;
				TIME += 1;
				break;
			case (0x16): // DEC @R0/@R1
			case (0x17):
				RAM[RAM[FEPROM[PC]-0x16]] = (RAM[RAM[FEPROM[PC]-0x16]] + 255) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x24): // ADD A, #c8
				A = A + FEPROM[PC+1];
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 2;
				TIME += 1;
				break;
			case (0x34): // ADDC A, #c8
				A = A + FEPROM[PC+1] + C;
				C = 0;
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 2;
				TIME += 1;
				break;
			case (0x28): // ADD A, Rn
			case (0x29):
			case (0x2A):
			case (0x2B):
			case (0x2C):
			case (0x2D):
			case (0x2E):
			case (0x2F):
				A = A + RAM[FEPROM[PC]-0x28];
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 1;
				TIME += 1;
				break;
			case (0x38): // ADDC A, Rn
			case (0x39):
			case (0x3A):
			case (0x3B):
			case (0x3C):
			case (0x3D):
			case (0x3E):
			case (0x3F):
				A = A + RAM[FEPROM[PC]-0x38] + C;
				C = 0;
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 1;
				TIME += 1;
				break;
			case (0x25): // ADD A, dadr
				A = A + get_data(FEPROM[PC+1]);
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 2;
				TIME += 1;
				break;
			case (0x35): // ADDC A, dadr
				A = A + get_data(FEPROM[PC+1]) + C;
				C = 0;
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 2;
				TIME += 1;
				break;
			case (0x26): // ADD A, @Rn
			case (0x27):
				A = A + RAM[RAM[FEPROM[PC]-0x26]];
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 1;
				TIME += 1;
				break;
			case (0x36): // ADDC A, @Rn
			case (0x37):
				A = A + RAM[RAM[FEPROM[PC]-0x36]] + C;
				C = 0;
				if (A > 255) {
					A = A % 256;
					C = 1;
				}
				PC += 1;
				TIME += 1;
				break;
			case (0x94): // SUBB A, #c8
				A = (A - FEPROM[PC+1] - C + 256) % 256;
				PC += 2;
				TIME += 1;
				break;
			case (0x95): // SUBB A, dadr
				A = (A - get_data(FEPROM[PC+1]) - C + 256) % 256;
				PC += 2;
				TIME += 1;
				break;
			case (0x98): // SUBB A, Rn
			case (0x99):
			case (0x9A):
			case (0x9B):
			case (0x9C):
			case (0x9D):
			case (0x9E):
			case (0x9F):
				A = (A - RAM[FEPROM[PC]-0x98] - C + 256) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0x96): // SUBB A, @Rn
			case (0x97):
				A = (A - RAM[RAM[FEPROM[PC]-0x98]] - C + 256) % 256;
				PC += 1;
				TIME += 1;
				break;
			case (0xC4): // SWAP A
				A = ((A >> 4) & 0b00001111) | ((A << 4) & 0b11110000);
				PC += 1;
				TIME += 1;
				break;
			case (0x23): // RL A
				ph = A & 0b10000000;
				A = A << 1;
				if (ph != 0)
					A = (A & 0b11111111) | 0b00000001;
				PC += 1;
				TIME += 1;
				break;
			case (0x03): // RR A
				ph = A & 0b00000001;
				A = A >> 1;
				if (ph != 0)
					A = (A & 0b11111111) | 0b10000000;
				PC += 1;
				TIME += 1;
				break;
			case (0xD3): // SETB C
				C = 1;
				PC += 1;
				TIME += 1;
				break;
			case (0xC3): // CLR C
				C = 0;
				PC += 1;
				TIME += 1;
				break;
			case (0xB3): // CPL C
				C = (C == 1 ? 0 : 1);
				PC += 1;
				TIME += 1;
				break;
			case (0xD2): // SETB badr
				set_badr(FEPROM[PC+1]);
				PC += 2;
				TIME += 1;
				break;
			case (0xC2): // CLR badr
				clr_badr(FEPROM[PC+1]);
				PC += 2;
				TIME += 1;
				break;
			case (0xB2): // CPL badr
				if (get_badr(FEPROM[PC+1]) != 0)
					clr_badr(FEPROM[PC+1]);
				else
					set_badr(FEPROM[PC+1]);
				PC += 2;
				TIME += 1;
				break;
			// LOGISCHE OPERATIONEN //
			case (0x54): // ANL A, #c8
				A = A & FEPROM[PC+1];
				PC += 2;
				TIME += 1;
				break;
			case (0x58): // ANL A, Rn
			case (0x59):
			case (0x5A):
			case (0x5B):
			case (0x5C):
			case (0x5D):
			case (0x5E):
			case (0x5F):
				A = A & RAM[FEPROM[PC]-0x58];
				PC += 1;
				TIME += 1;
				break;
			case (0x55): // ANL A, dadr
				if (FEPROM[PC+1] < 0x80)
					A = A & RAM[FEPROM[PC+1]];
				else
					A = A & RAM_SFR[FEPROM[PC+1]-0x80];
				PC += 2;
				TIME += 1;
				break;
			case (0x53): // ANL dadr, #c8
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+1]] & FEPROM[PC+2];
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+1]-0x80] & FEPROM[PC+2];
				PC += 3;
				TIME += 2;
				break;
			case (0x52): // ANL dadr, A
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+1]] & A;
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+1]-0x80] & A;
				PC += 2;
				TIME += 1;
				break;
			case (0x82): // ANL C, badr
				if (C != 0 && get_badr(FEPROM[PC+1]) != 0)
					C = 1;
				else
					C = 0;
				PC += 2;
				TIME += 2;
				break;
			case (0xB0): // ANL C, /badr
				if (C != 0 && get_badr(FEPROM[PC+1]) == 0)
					C = 1;
				else
					C = 0;
				PC += 2;
				TIME += 2;
				break;
			case (0x56): // ANL A, @Rn
			case (0x57):
				A = A & RAM[RAM[FEPROM[PC]-0x56]];
				PC += 1;
				TIME += 1;
				break;
			case (0x44): // ORL A, #c8
				A = A | FEPROM[PC+1];
				PC += 2;
				TIME += 1;
				break;
			case (0x43): // ORL dadr, #c8
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+1]] | FEPROM[PC+2];
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+1]-0x80] | FEPROM[PC+2];
				PC += 3;
				TIME += 2;
				break;
			case (0x48): // ORL A, Rn
			case (0x49):
			case (0x4A):
			case (0x4B):
			case (0x4C):
			case (0x4D):
			case (0x4E):
			case (0x4F):
				A = A | RAM[FEPROM[PC]-0x48];
				PC += 1;
				TIME += 1;
				break;
			case (0x45): // ORL A, dadr
				if (FEPROM[PC+1] < 0x80)
					A = A | RAM[FEPROM[PC+1]];
				else
					A = A | RAM_SFR[FEPROM[PC+1]-0x80];
				PC += 2;
				TIME += 1;
				break;
			case (0x42): // ORL dadr, A
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+1]] | A;
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+1]-0x80] | A;
				PC += 2;
				TIME += 1;
				break;
			case (0x72): // ORL C, badr
				if (C != 0 || get_badr(FEPROM[PC+1]) != 0)
					C = 1;
				else
					C = 0;
				PC += 2;
				TIME += 2;
				break;
			case (0xA0): // ORL C, /badr
				if (C != 0 || get_badr(FEPROM[PC+1]) == 0)
					C = 1;
				else
					C = 0;
				PC += 2;
				TIME += 2;
				break;
			case (0x46): // ORL A, @Rn
			case (0x47):
				A = A | RAM[RAM[FEPROM[PC]-0x46]];
				PC += 1;
				TIME += 1;
				break;
			case (0x64): // XRL A, #c8
				A = A ^ FEPROM[PC+1];
				PC += 2;
				TIME += 1;
				break;
			case (0x63): // XRL dadr, #c8
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+1]] ^ FEPROM[PC+2];
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+1]-0x80] ^ FEPROM[PC+2];
				PC += 3;
				TIME += 2;
				break;
			case (0x68): // XRL A, Rn
			case (0x69):
			case (0x6A):
			case (0x6B):
			case (0x6C):
			case (0x6D):
			case (0x6E):
			case (0x6F):
				A = A ^ RAM[FEPROM[PC]-0x68];
				PC += 1;
				TIME += 1;
				break;
			case (0x65): // XRL A, dadr
				if (FEPROM[PC+1] < 0x80)
					A = A ^ RAM[FEPROM[PC+1]];
				else
					A = A ^ RAM_SFR[FEPROM[PC+1]-0x80];
				PC += 2;
				TIME += 1;
				break;
			case (0x62): // XRL dadr, A
				if (FEPROM[PC+1] < 0x80)
					RAM[FEPROM[PC+1]] = RAM[FEPROM[PC+1]] ^ A;
				else
					RAM_SFR[FEPROM[PC+1]-0x80] = RAM_SFR[FEPROM[PC+1]-0x80] ^ A;
				PC += 2;
				TIME += 1;
				break;
			case (0x66): // XRL A, @Rn
			case (0x67):
				A = A ^ RAM[RAM[FEPROM[PC]-0x66]];
				PC += 1;
				TIME += 1;
				break;
			// SPRUNGBEFEHLE //
			case (0x02): // LJMP adr16
				PC = (FEPROM[PC + 1] << 8) | FEPROM[PC + 2];
				TIME += 2;
				break;
			case (0x80): // SJMP rel
				PC += 2 + FEPROM[PC+1];
				TIME += 2;
				break;
			case (0x73): // JMP @A+DPTR
				PC = A + DPTR;
				TIME += 2;
				break;
			case (0x10): // JBC badr, rel
				if (get_badr(FEPROM[PC+1]) != 0) {
					PC += 3 + FEPROM[PC+2];
				} else {
					PC += 3;
				}
				C = 0; // TODO stimmt das?
				TIME += 2;
				break;
			case (0x20): // JB badr, rel
				if (get_badr(FEPROM[PC+1]) != 0) {
					PC += 3 + FEPROM[PC+2];
				} else {
					PC += 3;
				}
				TIME += 2;
				break;
			case (0x30): // JNB badr, rel
				if (get_badr(FEPROM[PC+1]) == 0) {
					PC += 3 + FEPROM[PC+2];
				} else {
					PC += 3;
				}
				TIME += 2;
				break;
			case (0x40): // JC rel
				if (C != 0) {
					PC += 2 + FEPROM[PC+1];
				} else {
					PC += 2;
				}
				TIME += 2;
				break;
			case (0x50): // JNC rel
				if (C == 0) {
					PC += 2 + FEPROM[PC+1];
				} else {
					PC += 2;
				}
				TIME += 2;
				break;
			case (0x60): // JZ rel
				if (A == 0) {
					PC += 2 + FEPROM[PC+1]; // jump-root starts 2 registers later
					TIME += 2;
				} else {
					PC += 2;
					TIME += 2;
				}
				break;
			case (0x70): // JNZ rel
				if (A != 0) {
					PC += 2 + FEPROM[PC+1];
					TIME += 2;
				} else {
					PC += 2;
					TIME += 2;
				}
				break;
			case (0xD8): // DJNZ Rn, rel
			case (0xD9):
			case (0xDA):
			case (0xDB):
			case (0xDC):
			case (0xDD):
			case (0xDE):
			case (0xDF):
				RAM[FEPROM[PC]-0xD8] = (RAM[FEPROM[PC]-0xD8] + 255) % 256;
				if (RAM[FEPROM[PC]-0xD8] != 0) {
					PC += 2 + FEPROM[PC+1];
					TIME += 2;
				} else {
					PC += 2;
					TIME += 2;
				}
				break;
			case (0xD5): // DJNZ dadr, rel
				if (FEPROM[PC+1] < 0x80) {
					RAM[FEPROM[PC+1]] = (RAM[FEPROM[PC+1]] + 255) % 256;
					if (RAM[FEPROM[PC+1]] != 0)
						PC += 2 + FEPROM[PC+2];
					else
						PC += 2;
				} else {
					RAM_SFR[FEPROM[PC+1]-0x80] = (RAM_SFR[FEPROM[PC+1]-0x80] + 255) % 256;
					if (RAM_SFR[FEPROM[PC+1]-0x80] != 0)
						PC += 2 + FEPROM[PC+2];
					else
						PC += 2;
				}
				TIME += 2;
				break;
			case (0xB4): // CJNE A, #c8, rel
				if (A != FEPROM[PC+1])
					PC += 3 + FEPROM[PC+2];
				else
					PC += 3;
				TIME += 2;
				break;
			case (0xB8): // CJNE Rn, #c8, rel
			case (0xB9):
			case (0xBA):
			case (0xBB):
			case (0xBC):
			case (0xBD):
			case (0xBE):
			case (0xBF):
				if (RAM[FEPROM[PC]-0xB8] != FEPROM[PC+1])
					PC += 3 + FEPROM[PC+2];
				else
					PC += 3;
				TIME += 2;
				break;
			case (0xB5): // CJNE A, dadr, rel
				if (A != get_data(FEPROM[PC+1]))
					PC += 3 + FEPROM[PC+2];
				else
					PC += 3;
				TIME += 2;
				break;
			case (0xB6): // CJNE @Rn, #c8, rel
			case (0xB7):
				if (RAM[RAM[FEPROM[PC]-0xB6]] != FEPROM[PC+1])
					PC += 3 + FEPROM[PC+2];
				else
					PC += 3;
				TIME += 2;
				break;
			case (0x12): // LCALL adr16
				RAM[SP] = ((PC+3) & 0b1111111100000000) >> 8; // high bits
				RAM[SP+1] = (PC+3) & 0b0000000011111111; // low bits
				PC = (FEPROM[PC + 1] << 8) | FEPROM[PC + 2];
				SP += 2;
				TIME += 2;
				break;
			case (0x22): // RET
				SP = (SP + 254) % 256;
				PC = (RAM[SP] << 8) | RAM[(SP+1) % 256];
				TIME += 2;
				break;
      case (0x32): // RETI
        SP = (SP + 254) % 256;
        PC = (RAM[SP] << 8) | RAM[(SP+1) % 256];
        TIME += 2;
        set_data(0x88, get_data(0x88) & 0b11110101); // remove flags
        break;
			// BEFEHL EXISTIERT NICHT //
			default:
				dprint("Command " + FEPROM[PC] + " not implemented (yet)!", color(255, 0, 0));
				break;
		}
		
		if (PC >= FEPROM.length) {
			PC = 0;
			//dprint("PC was set to 0! (Exceeded FEPROM size)", color(255, 0, 0)); // spam
		}
		
		// Timers/interrupts
		deltaTime = TIME - deltaTime;
    
    int deltaP3 = (prevP3 ^ get_data(0xB0)) & 0b11111111; // XOR to get difference
    int deltaTCON = (prevTCON ^ get_data(0x88)) & 0b11111111;
    
    if (deltaP3 != 0)
      prevP3 = get_data(0xB0); // used for timer counter-mode
    if (deltaTCON != 0)
      prevTCON = get_data(0x88); // used for timer & external interrupts
    
		update_timers((int)deltaTime, deltaP3);
    update_interrupts(deltaP3, deltaTCON);
	}
	
	public int get_data(int adr) {
		if (adr < 0x80)
			return RAM[adr];
		else
			return RAM_SFR[adr-0x80];
	}
	
	public void set_data(int adr, int val) {
		if (adr < 0x80)
			RAM[adr] = val;
		else
			RAM_SFR[adr-0x80] = val;
	}
	
	public int get_badr(int badr) {
		int bit = badr % 8;
		return RAM_SFR[badr - 0x80 - bit] & (1 << bit);
	}
	
	public void set_badr(int badr) {
		int bit = badr % 8;
		RAM_SFR[badr - 0x80 - bit] = RAM_SFR[badr - 0x80 - bit] | (1 << bit);
	}
	
	public void clr_badr(int badr) {
		int bit = badr % 8;
		RAM_SFR[badr - 0x80 - bit] = RAM_SFR[badr - 0x80 - bit] & ((1 << bit) ^ 0b11111111);
	}
  
  public boolean is_bit(int d, int bit) {
    return (d & (1 << bit)) != 0;
  }
	
	private void update_timers(int deltaTime, int diffP3) {
		if (deltaTime == 0)
			return;
		
		int ph = 0; // placeholder to do random stuff with
		
		for (int t = 0; t < 2; t++) {
			if (get_badr(0x88 + 4 + (2 * t)) == 0) // ..skip if not running
				continue;
      
      int phDelta = deltaTime; // if not count-mode: == deltaTime; otherwise == 1
      if (is_bit(get_data(0x89), 2 + (4 * t))) // ..if count-mode..
      {
        phDelta = 1;
        
        if (!is_bit(diffP3, 4 + t) || is_bit(get_data(0xB0), 4 + t)) // ..continue if P3 did not change OR if it was set to 1
          continue;
      }
			
			int shift = t * 4;
			int tmod = (get_data(0x89) >> shift) & 0b1111;
			int mode = tmod & 0b0011;
			for (int dt = 0; dt < phDelta; dt++) {
				switch (mode) {
					case (0b01): // 16-Bit Timer ohne Nachladen
						ph = get_data(0x8A + t) | (get_data(0x8C + t) << 8);
						ph++;
						if (ph >= 65536) {
							ph &= 65536;
							set_badr(0x88 + 5 + (2 * t)); // Timer flag
						}
						set_data(0x8A + t, ph & 0b11111111);
						set_data(0x8C + t, (ph >> 8) & 0b11111111);
						break;
					case (0b10): // 8-Bit Timer mit Auto-Reload
						ph = get_data(0x8A + t);
						ph++;
						if (ph >= 256) {
							ph = get_data(0x8C + t);
							set_badr(0x88 + 5 + (2 * t)); // Timer flag
						}
						set_data(0x8A + t, ph);
						break;
          case (0b11): // 2x 8-Bit Timer
            // ..low
            ph = get_data(0x8A + t);
            ph++;
            if (ph >= 256) {
              ph = 0;
              set_badr(0x88 + 5 + (2 * t)); // Timer flag
            }
            set_data(0x8A + t, ph);
            // ..high
            ph = get_data(0x8C + t);
            ph++;
            if (ph >= 256) {
              ph = 0;
              set_badr(0x88 + 5 + (2 * t)); // Timer flag
            }
            set_data(0x8C + t, ph);
            break;
					default:
						dprint("Timer" + t + ": Mode " + mode + " not implemented yet!", color(255, 0, 0));
						break;
				}
			}
		}
	}
  
  private void update_interrupts(int deltaP3, int deltaTCON) {
    int IE = get_data(0xA8);
    if ((IE & 0b10000000) == 0)
      return;
    
    int TCON = get_data(0x88);
    int P3 = get_data(0xB0);
    
    
    if ((IE & 0b00000001) != 0) { // externer Interrupt 0
      if ((TCON & 0b00000001) != 0) {
        // ..int bei flanke 1-0
        if ((P3 & 0b00000100) == 0 && (deltaP3 & 0b00000100) != 0) {
          RAM[SP] = (PC & 0b1111111100000000) >> 8; // high bits
          RAM[SP+1] = PC & 0b0000000011111111; // low bits
          SP += 2;
          PC = 0x0003;
        }
      } else {
        // ..int bei lowpegel (P3.2 muss 0 sein + KEIN flag!)
        if ((P3 & 0b00000100) == 0 && (TCON & 0b00000010) == 0) {
          RAM[SP] = (PC & 0b1111111100000000) >> 8; // high bits
          RAM[SP+1] = PC & 0b0000000011111111; // low bits
          SP += 2;
          PC = 0x0003;
        }
      }
      
      // ..setze flag
      set_badr(0x88 + 1);
    }
    
    if ((IE & 0b00000100) != 0) { // externer Interrupt 1
      if ((TCON & 0b00000100) != 0) {
        // ..int bei flanke 1-0
        if ((P3 & 0b00001000) == 0 && (deltaP3 & 0b00001000) != 0) {
          RAM[SP] = (PC & 0b1111111100000000) >> 8; // high bits
          RAM[SP+1] = PC & 0b0000000011111111; // low bits
          SP += 2;
          PC = 0x0013;
        }
      } else {
        // ..int bei lowpegel (P3.3 muss 0 sein + KEIN flag!)
        if ((P3 & 0b00001000) == 0 && (TCON & 0b00001000) == 0) {
          RAM[SP] = (PC & 0b1111111100000000) >> 8; // high bits
          RAM[SP+1] = PC & 0b0000000011111111; // low bits
          SP += 2;
          PC = 0x0013;
        }
      }
      
      // ..setze flag
      set_badr(0x88 + 3);
    }
    
    if ((IE & 0b00000010) != 0) { // Timer0 Interrupt
      if ((TCON & 0b00100000) != 0) {
        // ..clear flag
        set_data(0x88, TCON & 0b11011111);
        // ..jump
        RAM[SP] = (PC & 0b1111111100000000) >> 8; // high bits
        RAM[SP+1] = PC & 0b0000000011111111; // low bits
        SP += 2;
        PC = 0x000B;
      }
    }
    
    if ((IE & 0b00001000) != 0) { // Timer1 Interrupt
      if ((TCON & 0b10000000) != 0) {
        // ..clear flag
        set_data(0x88, TCON & 0b01111111);
        // ..jump
        RAM[SP] = (PC & 0b1111111100000000) >> 8; // high bits
        RAM[SP+1] = PC & 0b0000000011111111; // low bits
        SP += 2;
        PC = 0x001B;
      }
    }
  }
}
