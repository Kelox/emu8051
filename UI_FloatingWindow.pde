public class UI_FloatingWindow extends UI_Widget {
	private UI_Widget child;
	
	
	public UI_FloatingWindow(float px, float py, float pw, float ph) {
		super(px, py, pw, ph);
		child = null;
	}
	
	public void draw() {
		// Draw Background
		fill(225);
		stroke(0);
		strokeWeight(1);
		rect(x, y, w, h, 16);
		
		// Draw child
		if (child != null) {
			translate(x, y);
			
			child.draw();
			
			translate(-x, -y);
		}
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		if (child != null && child.contains(px - x, py - y))
			child.released(px - x, py - y);
	}
	public void clicked(float px, float py) {
		if (child != null && child.contains(px - x, py - y))
			child.clicked(px - x, py - y);
	}
	public void dragged(float px, float py, float dx, float dy) {
		x += dx;
		y += dy;
	}
	public void keypressed(int k, int kcode) {
		
	}
	
	public void set_child(UI_Widget wid) {
		wid.x = 0;
		wid.y = 0;
		wid.w = w;
		wid.h = h;
		child = wid;
	}
}
