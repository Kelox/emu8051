# emu8051

8051 microcontroller emulator I made as a school project in Java/Processing. It should work on both PC and iOS.


## Using it on iOS

The Processing version on iOS acts weird very often, like for example when trying to overload methods, so there are some things that need to be changed first:

 * **emu8051.pde:** (At the very bottom) Remove all "strcmp"-methods, except "public boolean strcmp(String a, String b)"

 * **Compiler.pde:** Inside the constructor, remove "type_check();"

 * **UI_Widget.pde:** Remove the method "public boolean contains(int px, int py)". Keep the overloaded variant

 * **BEFORE IMPORTING:** Remove all files except those with the ".pde" extension


## Notes

On PC, it only seems to work with Processing 4. (P3 seems to have weird issues with bit-operations and array-elements)

This project was meant to be used on tablets, so the navigation is a bit odd on PC.


![images](images.gif)
