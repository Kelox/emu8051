public class UI_Scale extends UI_Widget {
	private String title;
	public float value;
	
	
	public UI_Scale(String pTitle, float pValue, int px, int py, int pw, int ph) {
		super(px, py, pw, ph);
		
		title = pTitle;
		value = pValue;
	}
	
	
	public void draw() {
		stroke(0, 0, 0, 0);
		fill(125, 125, 125, 255);
		rect(x, y, w, h, 16);
		fill(225, 225, 225, 255);
		stroke(0);
		rect(x, y, w * value, h, 16);
		
		textAlign(CENTER, CENTER);
		textSize(h);
		stext("-", x + h / 2, y + h / 2, color(255));
		stext("+", x + w - h / 2, y + h / 2, color(255));
		
		textAlign(CENTER, BOTTOM);
		textSize(h / 2);
		stext((int)round(value * 100) + "%", x + w / 2, y + h, color(255));
		
		textAlign(CENTER, TOP);
		textSize(h / 2.5);
		stext(title, x + w / 2, y, color(255));
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		px -= x;
		
		if (px < (w / 2))
			value -= 0.01f;
		else
			value += 0.01f;
		
		
		value = (value < 0 ? 0 : value);
		value = (value > 1 ? 1 : value);
	}
	public void dragged(float px, float py, float dx, float dy) {
		value += dx / 300;
		value = (value < 0 ? 0 : value);
		value = (value > 1 ? 1 : value);
	}
	public void keypressed(int k, int kcode) {
		
	}
}