public class UI_Button extends UI_Widget {
	private String title;
	private boolean toggle;
	private boolean toggle_state;
	
	public boolean was_pressed; // ignore if toggle is active
	
	// Animation
	private float anim_press;
	public color default_color;
	
	public UI_Button(String pTitle, boolean pToggle, float px, float py, float pw, float ph) {
		super(px, py, pw, ph);
		
		title = pTitle;
		toggle = pToggle;
		toggle_state = false;
		
		was_pressed = false;
		
		anim_press = 0;
		
		default_color = color(155, 155, 255);
	}
	
	public void draw() {
		// Shadow
		stroke(0, 0, 0, 0);
		fill(50, 50, 50, 50);
		rect(x, y + 5, w, h, 12);
		
		if (anim_press > 0)
			anim_press -= 0.8;
		
		
		// Button
		translate(0, anim_press);
		
		stroke(0);
		strokeWeight(3);
		fill(default_color);
		if (toggle) {
			if (toggle_state)
				fill(0, 255, 0);
			else
				fill(255, 0, 0);
		}
		rect(x, y, w, h, 12);
		
		// Shadow
		textSize(24);
		textAlign(CENTER, CENTER);
		fill(0);
		for (int xx = -1; xx < 2; xx++)
			for (int yy = -1; yy < 2; yy++)
				text(title, x + w / 2 + xx, y + h / 2 + yy);
		
		// Text
		fill(255);
		text(title, x + w / 2, y + h / 2);
		
		translate(0, -anim_press);
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		anim_press = 5;
		if (toggle)
			toggle_state = !toggle_state;
		else
			was_pressed = true;
	}
	public void dragged(float px, float py, float dx, float dy) {
		
	}
	public void keypressed(int k, int kcode) {
		
	}
	
	public boolean getState() {
		if (toggle)
			return toggle_state;
		else {
			boolean ph = was_pressed;
			was_pressed = false;
			return ph;
		}
	}
	public void setState(boolean p) {
		if (toggle)
			toggle_state = p;
		else
			was_pressed = p;
	}
}