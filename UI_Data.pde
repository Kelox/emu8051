public class UI_Data extends UI_Widget {
	private int[] list;
	public int start_y = 0;
	public float scroll = 0;
	public int offset = 0;
	public int marker = -1;
	
	public UI_Data(int[] pList) {
		super(0, 0, 0, 0);
		list = pList;
	}
	
	public void draw() {
		textSize(24);
		textAlign(LEFT, TOP);
		
		stroke(0, 0, 0, 0);
		fill(200, 200, 200, 100);
		rect(0, 0, textWidth("0000") + 16, h);
		
		float m = scroll / ((list.length / 8) - 1);
		float phh = h / 6;
		rect(w - 10, (h - phh) * m, 10, phh);
		
		fill(0);
		
		int end_y = (int)(start_y + (h / 48));
		if (end_y > (list.length / 8))
			end_y = list.length / 8;
		
		for (int phy = start_y; phy < end_y; phy++) {
			text(hex(phy * 8 + offset, 4), 8, (phy - start_y) * 48);
			for (int phx = 0; phx < 8; phx++) {
				int i = phy * 8 + phx;
				if (i < list.length)
					text(hex(list[i], 2), phx * 48 + 96, (phy - start_y) * 48);
				
				if (i == marker) {
					fill(255, 0, 0, 100);
					stroke(255, 0, 0);
					strokeWeight(1);
					rect(phx * 48 + 96, (phy - start_y) * 48, textWidth("FF"), 24);
					fill(0);
				}
			}
		}
	}
	public void pressed(float px, float py) {
		
	}
	public void released(float px, float py) {
		
	}
	public void clicked(float px, float py) {
		
	}
	public void dragged(float px, float py, float dx, float dy) {
		//dy *= 50;
		scroll -= (float)dy / 24f;
		scroll = (scroll < 0 ? 0 : scroll);
		scroll = (scroll > ((list.length / 8) - 1) ? ((list.length / 8) - 1) : scroll);
		
		start_y = (int)floor(scroll);
	}
	public void keypressed(int k, int kcode) {
		
	}
}
